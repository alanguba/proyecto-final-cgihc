# Proyecto final CGIHC

Como el proyecto está desarrollado en otro sistema operativo (Linux) deben asegurar que en sus máquinas compile sin errores:
*  Las diagonales deben de ir de acuerdo al sistema operativo que utilicen:
    #include <gtc\matrix_transform.hpp>
    #include <gtc\type_ptr.hpp>
* Las librerías y los archivos de imágenes deben estar bien referenciados:
    #include <glew.h>
    #include <glfw3.h>
    plainTexture = Texture("Textures/plain.png");
* Deben asegurarse de que su versión de OpenGL sea la correcta en el archivo Window.cpp:
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
