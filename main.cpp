/*
Proyecto final
 Alan Gutiérrez Bañuelos
 Uzziel Palma Rodríguez
 Federico Villegas Estrada
*/
//para cargar imagen
#define STB_IMAGE_IMPLEMENTATION

#include <stdio.h>
#include <string.h>
#include <cmath>
#include <vector>

#include<GL/glew.h>
#include <GLFW/glfw3.h>

#include "audio.h"

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include "Window.h"
#include "Mesh.h"
#include "Shader_light.h"
#include "Camera.h"
#include "Texture.h"
#include "Sphere.h"

//para iluminaciÛn
#include "CommonValues.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "Material.h"
#include"SpotLight.h"
//para elementos jerárquicos
#include "superman.h"
#include "kiosco.h"
#include "lumpy.h"

#include"Model.h"
#include "Skybox.h"

const float toRadians = 3.14159265f / 180.0f;

//variables de animación simple de auto
float movLlanta = 0.0f;
float movCocheX = -35.0f;
float movCocheZ = -49.0;
float movOffset = 1.8f;
float angRot = 0.0f;
float angOffset = 0.5f;
int parte = 0;

//variables animación simple superman
int animar = 3;
float giroSy = 0.0f;
float recorrido = 0.0f;
int ropa = 0;
bool finAnimacion = false;
GLfloat tiempo_transform = 0.0f;

//variables animacion simple Lumpy
float velocidadLF = 0.0f;
float giroLF = 0.0f;
float subida = 0.0f;
bool activaLF = false;

//variables animación compleja superman
bool caer = false;
float theta = 0.0f;
float curvatura = 0.0f;
float thetaOffset = 0.02f;

//variables animacion compleja Cosmo
int flotar = 0;
float epsilon = 1.0f;
float posCosmoX = 25.756f;
float posCosmoZ = 1.1f;
float posCosmoY = 1.0f;
float radio = 1.4f;
float giroCosmoY = 0.0f;
bool banderaCosmo = false;
bool banderaSound = false;


//variables espectaculo de luces
float pos_actLC = 0.0;
float pos_actL1 = 0.0;
float pos_actL2 = 0.0;
float pos_sigLC = 0.005;
float pos_sigL1 = 0.005;
float pos_sigL2 = -0.005;
float tonoColor = 0.0;
float rojo1 = 0.0;
float verde1 = 0.0;
float azul1 = 0.0;
float rojo2 = 0.0;
float verde2 = 0.0;
float azul2 = 0.0;
float tiempo = 0;
float relojtiempo = 0.005;

//variables para keyframe
float reproduciranimacion,habilitaranimacion;
//variables animacion avion
float posXavion = -20.0, posYavion = 4.0, posZavion = 0;
float movAvion_x = 0.0f, movAvion_y = 0.0f, movAvion_z = 0.0f;
float giroAvion = 0.0f;

//VARIABLES animación boomerang
float r;
float precalculo;
float phi;
float posxB = 20.0f;
float poszB = 0.0f;
float soyBatman = false;
float giroB = 0.0f;
float thetaOffset2 = 0.024f;
bool boomerangFijo = false;
bool boomerangMano = true;

//Variables Para Keyframe Superman
float reproduciranimacion2, habilitaranimacion2;
//variables animacion Superman
float posX = 3.0f, posY = 0.55f, posZ = -20.55f;
float movSup_x = 0.0f, movSup_z = 0.0f;
float giroPIzq = 0.0f, giroPDer = 0.0f;
float giroBIzq = 0.0f, giroBDer = 0.0f;
float giroSup = 0.0f;
float puerta = -90.0f;

//Variables Para Keyframe Lumpy
float reproduciranimacion3, habilitaranimacion3;
//variables animacion Lumpy
float posXL = 20.0f, posYL = 0.55f, posZL = -3.0f;
float movLum_x = 0.0f, movLum_z = 0.0f, movLum_y = 0.0f;
float giroPIzqL = 0.0f, giroPDerL = 0.0f;
float giroBIzqL = 0.0f, giroBDerL = -90.0f;
float giroLumY = 0.0f, giroLumZ = 0.0f, giroLumX;

//Variables Para Keyframe Wanda
float reproduciranimacion4, habilitaranimacion4;
//variables animación Wanda
float posXW = 25.0f, posYW = 1.0f, posZW = 0.5f;
float movXW = 0.0f, movYW = 0.0f, movZW = 0.0f;
float giroYW = 0.0f;

Sphere sp = Sphere(20, 20, 20);

//Estructua keyframes
#define MAX_FRAMES 34 //9
#define MAX_FRAMES2 27 //Superman
#define MAX_FRAMES3 18 //Lumpy
#define MAX_FRAMES4 13 //Wanda
int i_max_steps = 90; //número de inbetweens
int i_curr_steps = 34; //número de keyframes guardados en memoria declarados a mano. Se graudan de 5 a 0
int i_max_steps_2 = 90; //número de inbetweens para Superman
int i_curr_steps_2 = 27; //número de keyframes guardados en memoria declarados a mano. Se graudan de 5 a 0 para Superman
int i_max_steps_3 = 90; //número de inbetweens para Lumpy
int i_curr_steps_3 = 18; //número de keyframes guardados en memoria declarados a mano. Se graudan de 5 a 0 para Lumpy
int i_max_steps_4 = 90; //número de inbetweens para Wanda
int i_curr_steps_4 = 13;

typedef struct _frame
{
    //Variables para GUARDAR Key Frames Por cada variable debe de haber un incremento
    float movAvion_x;        //Variable para PosicionX
    float movAvion_y;        //Variable para PosicionY
    float movAvion_z;        //Variable para PosicionZ
    float movAvion_xInc;        //Variable para IncrementoX
    float movAvion_yInc;        //Variable para IncrementoY
    float movAvion_zInc;        //Variable para IncrementoZ
    float giroAvion;
    float giroAvionInc;
}FRAME;

typedef struct _frame2
{
    //Variables para GUARDAR Key Frames Por cada variable debe de haber un incremento
    float movSup_x;        //Variable para PosicionX
    float movSup_z;        //Variable para PosicionZ
    float giroSup;          //Giro de Superman
    float giroPIzq;
    float giroPDer;
    float giroBIzq;
    float giroBDer;
    float puerta;
    float movSup_xInc;        //Variable para IncrementoX
    float movSup_zInc;        //Variable para IncrementoZ
    float giroSupInc;       //Incremento para Giro
    float giroPIzqInc;
    float giroPDerInc;
    float giroBIzqInc;
    float giroBDerInc;
    float puertaInc;

}FRAME2;

typedef struct _frame3
{
    //Variables para GUARDAR Key Frames Por cada variable debe de haber un incremento
    float movLum_x;        //Variable para PosicionX
    float movLum_y;        //Variable para PosicionY
    float movLum_z;        //Variable para PosicionZ
    float giroLumY;          //Giro de Lumpy en Y
    float giroLumX;          //Giro de Lumpy en Y
    float giroLumZ;          //Giro de Lumpy en Z
    float giroPIzqL;
    float giroPDerL;
    float giroBIzqL;
    float giroBDerL;
    float movLum_xInc;        //Variable para IncrementoX
    float movLum_yInc;        //Variable para IncrementoY
    float movLum_zInc;        //Variable para IncrementoZ
    float giroLumYInc;       //Incremento para Giro en Y
    float giroLumXInc;       //Incremento para Giro en X
    float giroLumZInc;       //Incremento para Giro en Z
    float giroPIzqLInc;
    float giroPDerLInc;
    float giroBIzqLInc;
    float giroBDerLInc;

}FRAME3;

//Estructura KeyFrames Wanda
typedef struct _frame4
{
    float movXW;
    float movYW;
    float movZW;
    float giroYW;
    float movXW_Inc;        //Variable para IncrementoX
    float movYW_Inc;        //Variable para IncrementoY
    float movZW_Inc;        //Variable para IncrementoZ
    float giroYW_Inc;       //Incremento para Giro en Y
}FRAME4;

FRAME KeyFrame[MAX_FRAMES];
FRAME2 KeyFrame2[MAX_FRAMES2];
FRAME3 KeyFrame3[MAX_FRAMES3];
FRAME4 KeyFrame4[MAX_FRAMES4];
int FrameIndex = 34;            //numero de frame
bool play = false;
int playIndex = 0;
//Superman
int FrameIndex2 = 27;            //numero de frame
bool play2 = false;
int playIndex2 = 0;
//Lumpy
int FrameIndex3 = 18;            //numero de frame
bool play3 = false;
int playIndex3 = 0;

//Wanda
int FrameIndex4 = 13;            //numero de frame
bool play4 = false;
int playIndex4 = 0;

Window mainWindow;
std::vector<Mesh*> meshList;
std::vector<Shader> shaderList;
Camera camera;

Audio sound;  //música de fondo
Audio effect; //efecto de las helices
Audio wind;   //efecto del viento
Audio repito; //efecto de Cosmo

//Variables de textura
Texture brickTexture;
Texture dirtTexture;
Texture plainTexture;
Texture T_super;
Texture T_slumpy;
Texture T_slumpyF;
Texture faceTexture;
Texture caraLumpy;
Texture caraLumpyF;
Texture caraLumpyM;
Texture pielTexture;
Texture T_rojo;
Texture T_cuernos;
Texture T_cuernosF;
Texture roundBrickTexture;
Texture brick241Texture;
Texture brick223Texture;
Texture camino_T;
Texture pasto_T;
Texture acera_T;
Texture calle_T;
Texture esquina_calle_T;
Texture loceta_T;
Texture faro_T;
Texture bote_T;
Texture banoHTexture;
Texture banoMTexture;
Texture flash_T;
Texture lex_T;
Texture facesorp_T;
Texture onomatopeya_T;
Texture cabina_T;
Texture puerta_T;
Texture agua_T;


//materiales
Material Material_brillante;
Material Material_opaco;

//luz direccional
DirectionalLight mainLight;

//para declarar varias luces de tipo pointlight
PointLight pointLights[MAX_POINT_LIGHTS];
SpotLight spotLights[MAX_SPOT_LIGHTS];

//modelos
Model arbusto_M;    //para cargar ARBUSTOS
Model arbol_M;      //para cargar ÁRBOLES
Model reja_M;       //para cargar REJAS
Model llanta_M;     //para cargar LLANTAS
Model auto_M;       //para cargar AUTO
Model decaedro_M;   //para cargar DECAEDRO
Model Blackhawk_M;  //para cargar HELICÓPTERO
Model fuente_M;     //para cargar FUENTES
Model lantern_M;    //para cargar LUMINARIAS
Model wanda_M;      //para cargar WANDA
Model cosmo_M;      //para cargar COSMO
Model pecera_M;     //para cargar castillo
Model halo_M;       //para cargar halo

FILE * fp;
//FILE * fp2;

void inputKeyframes(bool* keys);

GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;
GLuint uniformModel = 0;

// Vertex Shader
static const char* vShader = "Shaders/shader_light.vert";

// Fragment Shader
static const char* fShader = "Shaders/shader_light.frag";
//c·lculo del promedio de las normales para sombreado de Phong
void calcAverageNormals(unsigned int * indices, unsigned int indiceCount, GLfloat * vertices, unsigned int verticeCount,
                        unsigned int vLength, unsigned int normalOffset)
{
    for (size_t i = 0; i < indiceCount; i += 3)
    {
        unsigned int in0 = indices[i] * vLength;
        unsigned int in1 = indices[i + 1] * vLength;
        unsigned int in2 = indices[i + 2] * vLength;
        glm::vec3 v1(vertices[in1] - vertices[in0], vertices[in1 + 1] - vertices[in0 + 1], vertices[in1 + 2] - vertices[in0 + 2]);
        glm::vec3 v2(vertices[in2] - vertices[in0], vertices[in2 + 1] - vertices[in0 + 1], vertices[in2 + 2] - vertices[in0 + 2]);
        glm::vec3 normal = glm::cross(v1, v2);
        normal = glm::normalize(normal);
        
        in0 += normalOffset; in1 += normalOffset; in2 += normalOffset;
        vertices[in0] += normal.x; vertices[in0 + 1] += normal.y; vertices[in0 + 2] += normal.z;
        vertices[in1] += normal.x; vertices[in1 + 1] += normal.y; vertices[in1 + 2] += normal.z;
        vertices[in2] += normal.x; vertices[in2 + 1] += normal.y; vertices[in2 + 2] += normal.z;
    }

    for (size_t i = 0; i < verticeCount / vLength; i++)
    {
        unsigned int nOffset = i * vLength + normalOffset;
        glm::vec3 vec(vertices[nOffset], vertices[nOffset + 1], vertices[nOffset + 2]);
        vec = glm::normalize(vec);
        vertices[nOffset] = vec.x; vertices[nOffset + 1] = vec.y; vertices[nOffset + 2] = vec.z;
    }
}

/*
 Primitivas:
 0 - Piso
 1 - Cabello
 2 - Cabeza
 3 - Torzo
 4 - Brazo Izquierdo
 5 - Brazo Derecho
 6 - Mano
 7 - Cadera
 8 - Pierna
 9 - Capa
 10 - Cuernos
 11 - Fence
 12 - RoundBrick
 13 - Bloque 2x2x3
 14 - Bloque 2x4x1
 15 - Cubo
 16 - Bloque 1x1x1
 17 - Calle
 18 - Esquina acera
 19 - Esquina calle
 20 - Esquina cuadro
 21 - Faro
 22 - Bote
 23 - Baños H/M
 24 - Plano
 25.- Cabina
 26.- Puerta
 27 - Boomerang
 28 - Alas
 */

/* Función para crear aceras y pasto*/
void CrearPiso(){
    unsigned int floorIndices[] = {
        0, 2, 1,
        1, 2, 3
    };

    //piso de 16x30
    GLfloat floorVertices[] = {
        -8.0f,  0.0f,   -15.0f,     0.0f,   0.0f,           0.0f, -1.0f, 0.0f,
        8.0f,   0.0f,   -15.0f,     8.0f,  0.0f,          0.0f, -1.0f, 0.0f,//16
        -8.0f,  0.0f,   15.0f,      0.0f,   30.0f,         0.0f, -1.0f, 0.0f,//30-2
        8.0f,   0.0f,   15.0f,      8.0f,  30.0f,         0.0f, -1.0f, 0.0f//16 y 30
    };
    Mesh *piso = new Mesh();
    piso->CreateMesh(floorVertices, floorIndices, 32, 6);
    meshList.push_back(piso);
}

/*Función para el plano*/
void crearPlano(){
    unsigned int floorIndices[] = {
        0,1,2,
        2,3,0
    };

    //piso de 16x30
    GLfloat floorVertices[] = {
        -0.5f,  -0.5f,   0.0f,     0.0f,   0.0f,           0.0f, 0.0f, -1.0f,
        0.5f,   -0.5f,   0.0f,     1.0f,  0.0f,          0.0f, 0.0f, -1.0f,//16
        0.5f,  0.5f,   0.0f,      1.0f,   1.0f,         0.0f, 0.0f, -1.0f,//30-2
        -0.5f,   0.5f,   0.0f,      0.0f,  1.0f,         0.0f, 0.0f, -1.0f//16 y 30
    };
    Mesh *piso = new Mesh();
    piso->CreateMesh(floorVertices, floorIndices, 32, 6);
    meshList.push_back(piso);
    
    //Crear cabina
    Mesh* cabina = new Mesh();
    cabina->CreateMesh(cabina_vertices, cabina_indices, 320, 60); //Todo es correcto
    meshList.push_back(cabina);

    //Crear puerta
    Mesh* puerta = new Mesh();
    puerta->CreateMesh(puerta_vertices, puerta_indices, 192, 36); //Todo es correcto
    meshList.push_back(puerta);
}

/* Funcion para crear alas*/
void CreateWings() {
    unsigned int indices_wings[] = {
        11, 0, 1,
        11, 1, 2,
        11, 2, 3,
        11, 3, 4,
        11, 4, 5,
        11, 5, 10,
        9, 10, 5,
        9, 5, 6,
        9, 6, 7,
        9, 7, 8, //10

        23, 12, 13,
        23, 13, 14,
        23, 14, 15,
        23, 15, 16,
        23, 16, 17,
        23, 17, 22,
        21, 22, 17,
        21, 17, 18,
        21, 18, 19,
        21, 19, 20, //20
    };

    GLfloat vertices_wings[] = {
        //ala 1
        0.1f, -0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //0
        0.3f, -0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.5f, 0.0f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.55f, 0.1f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.65f, 0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.85f, 0.25f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        1.0f, 0.3f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        1.1f, 0.4f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        1.2f, 0.6f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        1.0f, 0.5f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.5f, 0.5f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        0.0f, 0.1f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //11

        //ala 2
        -0.1f, -0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //12
        -0.3f, -0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.5f, 0.0f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.55f, 0.1f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.65f, 0.2f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.85f, 0.25f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -1.0f, 0.3f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -1.1f, 0.4f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -1.2f, 0.6f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -1.0f, 0.5f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.5f, 0.5f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
        -0.0f, 0.1f, 0.0f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //23
    };

    Mesh* wing = new Mesh();
    wing->CreateMesh(vertices_wings, indices_wings, 192, 60);
    meshList.push_back(wing);
}

/*Función para crear Batarang*/
void CreateBoomerang() {
    unsigned int indices_boomerang[] = {
        0, 2, 1,
        3, 1, 2,
        3, 2, 4,
        5, 4, 2,
        5, 2, 6,

        8, 5, 7,
        5, 6, 7,
        9, 8, 10,
        10, 8, 7,
        11, 9, 10,
        12, 11, 10,
        13, 12, 10, //12

        14, 16, 15,
        17, 15, 16,
        17, 16, 18,
        19, 18, 16,
        19, 16, 20,

        22, 19, 21,
        19, 20, 21,
        23, 22, 24,
        24, 22, 21,
        25, 23, 24,
        26, 25, 24,
        27, 26, 24, //24

        //uniones
        28, 29, 30,
        30, 31, 28, //26
        32, 33, 34,
        34, 35, 32, //28
        36, 37, 38,
        38, 39, 36, //30
        40, 41, 42,
        42, 43, 40, //32
        44, 45, 46,
        46, 47, 44, //34
        48, 49, 50,
        50, 51, 48, //36
        52, 53, 54,
        54, 55, 52, //38
        56, 57, 58,
        58, 59, 56, //40
        60, 61, 62,
        62, 63, 60, //42
        64, 65, 66,
        66, 67, 64,
        68, 69, 70,
        70, 71, 68,
        72, 73, 74,
        74, 75, 72,
        76, 77, 78,
        78, 79, 76,
        80, 81, 82,
        82, 83, 80, //52

    };
    GLfloat vertices_boomerang[] = {
        //    x      y      z            u      v            nx      ny    nz
            //derecha
            1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //0
            0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //6

            //izquierda
            -0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //7
            -0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            -0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            -0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            -0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            -0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f,
            -1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, 0.0f, -1.0f, //13

            //derecha  2
            1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f, //14
            0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f,1.0f,
            0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f, //20

            //izquierda  2
            -0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f, //21
            -0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            -0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            -0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            -0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            -0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f,
            -1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, 0.0f, 1.0f, //27

            //uniones
            1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f, //28
            1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,

            1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //32
            0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f,0.0f,
            1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,

            0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f,0.0f, // 39

            0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //43

            0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //47

            0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f, //51

            //middle
            -0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f, //55

            -0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //59

            //
            -1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f, //63

            -1.0f, 0.0f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f,0.0f,
            -1.0f, 0.0f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //67

            -0.7f, -0.05f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.7f, -0.05f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f,0.0f, // 71

            -0.6f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.6f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //75

            -0.35f, -0.1f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.1f, -0.2f, 0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.1f, -0.2f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f,
            -0.35f, -0.1f, -0.1f,    0.0f, 0.0f,        0.0f, 1.0f, 0.0f, //79

            -0.1f, 0.1f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.4f, 0.2f, 0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.4f, 0.2f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f,
            -0.1f, 0.1f, -0.1f,    0.0f, 0.0f,        0.0f, -1.0f, 0.0f, //83

    };

    Mesh* boomerang = new Mesh();
    boomerang->CreateMesh(vertices_boomerang, indices_boomerang, 672, 156);
    meshList.push_back(boomerang);

}


/* Función para crear calle*/
void CrearCalle(){
    unsigned int floorIndices[] = {
        0, 2, 1,
        1, 2, 3
    };

    GLfloat floorVertices[] = {
        -6.0f,  0.0f,   -38.0f,     0.016f,   0.0f,           0.0f, -1.0f, 0.0f,
        6.0f,   0.0f,   -38.0f,     1.0f,  0.0f,          0.0f, -1.0f, 0.0f,//16
        -6.0f,  0.0f,   38.0f,      0.0f,   6.0f,         0.0f, -1.0f, 0.0f,//30-2
        6.0f,   0.0f,   38.0f,      1.0f,  6.0f,         0.0f, -1.0f, 0.0f//16 y 30
    };
    Mesh *piso = new Mesh();
    piso->CreateMesh(floorVertices, floorIndices, 32, 6);
    meshList.push_back(piso);
}

void CrearEsquina(){
    //esquina acera
    unsigned int floorIndices[] = {
        0, 1, 2,
        2, 3, 0,
        7, 4, 5,
        7, 5, 6,
    };

    GLfloat floorVertices[] = {
        -8.0f,  0.0f,   20.0f,     0.0f,   0.0f,           0.0f, -1.0f, 0.0f,
        0.0f,   0.0f,   20.0f,     4.0f,  0.0f,          0.0f, -1.0f, 0.0f,
        0.0f,  0.0f,   0.0f,       4.0f,   20.0f,         0.0f, -1.0f, 0.0f,
        -8.0f,   0.0f,   0.0f,    0.0f,  20.0f,         0.0f, -1.0f, 0.0f,
        20.0f,   0.0f,   0.0f,      12.0f,  0.0f,         0.0f, -1.0f, 0.0f,
        20.0f,   0.0f,   -8.0f,      12.0f,  8.0f,         0.0f, -1.0f, 0.0f,
        -8.0f,   0.0f,   -8.0f,      0.0f,  8.0f,         0.0f, -1.0f, 0.0f,
        -8.0f,   0.0f,   0.0f,    0.0f,  0.0f,         0.0f, -1.0f, 0.0f,
        
    };
    Mesh *piso = new Mesh();
    piso->CreateMesh(floorVertices, floorIndices, 64, 12);
    meshList.push_back(piso);
    
    //esquina calle
    unsigned int floor_2Indices[] = {
        0, 1, 2,
        2, 3, 0,
        7, 8, 6,
        8, 9, 6,
        8, 4, 9,
        4, 5, 9,
        
        
    };
    
    GLfloat floor_2Vertices[] = {
        -12.0f,  0.0f,   12.0f,     0.0f,   0.0f,           0.0f, -1.0f, 0.0f, //c
        0.0f,   0.0f,   12.0f,     0.5488f,  0.01f,          0.0f, -1.0f, 0.0f, //c
        0.0f,  0.0f,   4.0f,       0.5488f,   0.4316f,         0.0f, -1.0f, 0.0f, //c
        -12.0f,   0.0f,   4.0f,    0.0f,  0.4316f,         0.0f, -1.0f, 0.0f, //c
        8.0f,   0.0f,   4.0f,      1.0f,  0.4316f,         0.0f, -1.0f, 0.0f,
        8.0f,   0.0f,   -8.0f,      0.99f,  0.99f,         0.0f, -1.0f, 0.0f,
        -12.0f,   0.0f,   -8.0f,      0.0f,  0.99f,         0.0f, -1.0f, 0.0f,
        -12.0f,   0.0f,   4.0f,    0.0f,  0.4316f,         0.0f, -1.0f, 0.0f, //3 - r c
        0.0f,   0.0f,   4.0f,    0.5488f,  0.4316f,         0.0f, -1.0f, 0.0f, //2 - r c
        0.0f,   0.0f,   -8.0f,    0.5488f,  0.99f,         0.0f, -1.0f, 0.0f, // 2 parallel
        
    };
    Mesh *esquina = new Mesh();
    esquina->CreateMesh(floor_2Vertices, floor_2Indices, 80, 18);
    meshList.push_back(esquina);
    
    //esquina inferior rectangulo
    unsigned int floor_3Indices[] = {
        0, 1, 2,
        2, 3, 0,
    };

    GLfloat floor_3Vertices[] = {
        -4.0f,  0.0f,   -4.0f,     0.0f,   0.0f,           0.0f, -1.0f, 0.0f,
        4.0f,   0.0f,   -4.0f,     4.0f,  0.0f,          0.0f, -1.0f, 0.0f,
        4.0f,  0.0f,   4.0f,       4.0f,   8.0f,         0.0f, -1.0f, 0.0f,
        -4.0f,   0.0f,   4.0f,    0.0f,  8.0f,         0.0f, -1.0f, 0.0f,

        
    };
    Mesh *esq_rect = new Mesh();
    esq_rect->CreateMesh(floor_3Vertices, floor_3Indices, 64, 6);
    meshList.push_back(esq_rect);
}

void CrearObjetos()
{
    unsigned int faroIndices[] = {
        0, 1, 2,
        0, 2, 3,
        4,5,6,
        4,6,7
    };

    GLfloat faroVertices[] = {
        -0.5f, -0.5f, 0.0f,        0.0f, 0.0f,        0.0f, 0.0f, 0.0f,
        0.5f, -0.5f, 0.0f,        1.0f, 0.0f,        0.0f, 0.0f, 0.0f,
        0.5f, 0.5f, 0.0f,        1.0f, 1.0f,        0.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, 0.0f,        0.0f, 1.0f,        0.0f, 0.0f, 0.0f,

        0.0f, -0.5f, -0.5f,        0.0f, 0.0f,        0.0f, 0.0f, 0.0f,
        0.0f, -0.5f, 0.5f,        1.0f, 0.0f,        0.0f, 0.0f, 0.0f,
        0.0f, 0.5f, 0.5f,        1.0f, 1.0f,        0.0f, 0.0f, 0.0f,
        0.0f, 0.5f, -0.5f,        0.0f, 1.0f,        0.0f, 0.0f, 0.0f,


    };
    
    unsigned int boteIndices[] = {
        0, 1, 2,
        2, 3, 0,
        
        4, 5, 6,
        6, 7, 4,
        
        8, 9, 10,
        10, 11, 8,
        
        12, 13, 14,
        14, 15, 12,
        
        16, 17, 18,
        18, 19, 16,
    };

    GLfloat boteVertices[] = {
        
        //front
        -0.5f, -0.5f, 0.5f,     0.01f, 0.0f, 0.0f, 0.0f, -1.0f,
        0.5f, -0.5f, 0.5f,      0.45f, 0.0f, 0.0f, 0.0f, -1.0f,
        0.5f, 0.5f, 0.5f,       0.45f, 1.0f, 0.0f, 0.0f, -1.0f,
        -0.5f, 0.5f, 0.5f,      0.01f, 1.0f, 0.0f, 0.0f, -1.0f,
        
        //left
        -0.5f, -0.5f, -0.5f,    0.535f, 0.0f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, 0.5f,     0.967f, 0.0f, 1.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, 0.5f,      0.967f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, -0.5f,     0.533f, 1.0f, 1.0f, 0.0f, 0.0f,
        
        //right
        0.5f, -0.5f, -0.5f,    0.535f, 0.0f, -1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, 0.5f,     0.967f, 0.0f, -1.0f, 0.0f, 0.0f,
        0.5f, 0.5f, 0.5f,      0.967f, 1.0f, -1.0f, 0.0f, 0.0f,
        0.5f, 0.5f, -0.5f,     0.533f, 1.0f, -1.0f, 0.0f, 0.0f,
        
        //back
        -0.5f, -0.5f, -0.5f,    0.535f, 0.0f, 0.0f, 0.0f, 1.0f,
        0.5f, -0.5f, -0.5f,     0.967f, 0.0f, 0.0f, 0.0f, 1.0f,
        0.5f, 0.5f, -0.5f,      0.967f, 1.0f, 0.0f, 0.0f, 1.0f,
        -0.5f, 0.5f, -0.5f,     0.533f, 1.0f, 0.0f, 0.0f, 1.0f,
        
        //bottom
        -0.5f, -0.5f, 0.5f,    0.63f, 0.34f, 0.0f, -1.0f, 0.0f,
        0.5f, -0.5f, 0.5f,     0.87f, 0.34f, 0.0f, -1.0f, 0.0f,
        0.5f, -0.5f, -0.5f,      0.87f, 0.9f, 0.0f, -1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,     0.63f, 0.9f, 0.0f, -1.0f, 0.0f,

    };
    
    calcAverageNormals(faroIndices, 12, faroVertices, 64, 8, 5);

    Mesh *faro = new Mesh();
    faro->CreateMesh(faroVertices, faroIndices, 64, 12);
    meshList.push_back(faro);
    
    Mesh *bote = new Mesh();
    bote->CreateMesh(boteVertices, boteIndices, 160, 30);
    meshList.push_back(bote);
}


/* Función para crear el cuerpo de Superman */
void crearSuperman()
{
    //Crear cabello
    Mesh *cabello = new Mesh();
    cabello->CreateMesh(vertices_cabello_sup, indices_cabello_sup,3704, 1779);
    meshList.push_back(cabello);
    
    //Crear cabeza
    Mesh *cabeza = new Mesh();
    cabeza->CreateMesh(vertices_cabeza_sup, indices_cabeza_sup, 3840, 672);
    meshList.push_back(cabeza);
    
    //Crear torzo
    Mesh *torzo = new Mesh();
    torzo->CreateMesh(vertices_torzo_sup, indices_torzo_sup, 192, 36);
    meshList.push_back(torzo);
    
    //Crear brazo izquierdo
    Mesh *brazo_izq = new Mesh();
    brazo_izq->CreateMesh(vertices_brazo_izq_sup, indices_brazo_izq_sup,3568, 663);
    meshList.push_back(brazo_izq);

    //Crear brazo derecho
    Mesh *brazo_der = new Mesh();
    brazo_der->CreateMesh(vertices_brazo_der_sup, indices_brazo_der_sup,3568, 663);
    meshList.push_back(brazo_der);
    
    //Crear mano
    Mesh *mano = new Mesh();
    mano->CreateMesh(vertices_mano_sup, indices_mano_sup, 2624, 444);
    meshList.push_back(mano);
    
    //Crear cadera
    Mesh *cadera = new Mesh();
    cadera->CreateMesh(vertices_cadera_sup, indices_cadera_sup,1824, 1239);
    meshList.push_back(cadera);
    
    //Crear pierna
    Mesh *pierna = new Mesh();
    pierna->CreateMesh(vertices_piernas_sup, indices_piernas_sup, 592, 162);
    meshList.push_back(pierna);
    
    //Crear capa
    Mesh *capa = new Mesh();
    capa->CreateMesh(vertices_capa_sup, indices_capa_sup, 1152, 144);
    meshList.push_back(capa);
}

/* Función para crear el cuerpo de Lumpy */

void crearLumpy()
{
    //Crear cuerno
    Mesh* cuernos = new Mesh();
    cuernos->CreateMesh(vertices_cuernos_lumpy, indices_cuernos_lumpy, 3584, 576);
    meshList.push_back(cuernos);
}

/* Función para crear las partes del kiosco */

void crearBloquesKiosco()
{
    //Crear valla
    Mesh *fence = new Mesh();
    fence->CreateMesh(fence_vertices, fence_indices,3424, 1848);
    meshList.push_back(fence);
    
    //Crear bloque cilíndrico
    Mesh* roundBrick = new Mesh();
    roundBrick->CreateMesh(roundBrickVertices, roundBrickIndices, 4480, 720); //Todo es correcto
    meshList.push_back(roundBrick);
    
    //Crear bloque 2x2x3
    Mesh* brick223 = new Mesh();
    brick223->CreateMesh(brick223Vertices, brick223Indices, 3744, 606); //Todo es correcto
    meshList.push_back(brick223);

    //Crear bloque 2x4x1
    Mesh* brick241 = new Mesh();
    brick241->CreateMesh(brick241Vertices, brick241Indices, 7328, 1182); //Todo es correcto
    meshList.push_back(brick241);

    //Crear Cubo

    Mesh* cubo = new Mesh();
    cubo->CreateMesh(cubo_vertices, cubo_indices, 192, 36);
    meshList.push_back(cubo);

    //Crear bloque 1x1x1
    Mesh* brick111 = new Mesh();
    brick111->CreateMesh(brick111Vertices, brick111Indices, 1088, 180); //Todo es correcto
    meshList.push_back(brick111);

}

void CrearBano()
{
    unsigned int bano_indices[] = {
        //cara lego
        // bottom
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
        9, 10, 11,
        12, 13, 14,
        15, 16, 17,
        18, 19, 20,
        21, 22, 23,

        24, 25, 26,
        25, 26, 27,
        28, 29, 30,
        29, 30, 31,
        32, 33, 34,
        33, 34, 35,
        36, 37, 38,
        37, 38, 39,
        40, 41, 42,
        41, 42, 43,
        44, 45, 46,
        45, 46, 47,
        48, 49, 50,
        49, 50, 51,
        52, 53, 54,
        53, 54, 55,

        56, 57, 58,
        59, 60, 61,
        62, 63, 64,
        65, 66, 67,
        68, 69, 70,
        71, 72, 73,
        74, 75, 76,
        77, 78, 79,

        80, 81, 82,
        80, 82, 83,
        84, 85, 86,
        84, 86, 87,
        88, 89, 90,
        88, 90, 91,
        92, 93, 94,
        92, 94, 95,
        96, 97, 98,
        96, 98, 99

    };
    GLfloat bano_vertices[] = {

        


       //Frente Semicircular
//1
               //x        y        z        S        T
        0.0, 0.0f,  0.3f,          0.8f,    1.0f,        0.0f,    0.0f,    -1.0f,//0
        0.25f,0.0f,0.3f,          0.8f,    1.0f,        0.0f,    0.0f,    -1.0f,
        0.231695f, 0.106695f,0.3f,0.8f,    1.0f,        0.0f,    0.0f,    -1.0f,
//2
        0.0, 0.0f, 0.3f,           0.8f, 1.0f,        0.0f,    0.0f,    -1.0f,
        0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        0.0f,    0.0f,    -1.0f,
        0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,   -1.0f,
//3
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,    -1.0f,
        0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,   -1.0f,
        0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,   -1.0f,
//4
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,    -1.0f,
        0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,   -1.0f,
        0.0f, 0.25f,0.3f,           0.8f, 1.0f,        0.0f,    0.0f,   -1.0f,
//5
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,    -1.0f,
        0.0f, 0.25f,0.3f,           0.8f,  1.0f,        0.0f,    0.0f,   -1.0f,
       -0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,   -1.0f,
//6
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,
       -0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    -1.0f,
       -0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    -1.0f,
//7
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,
       -0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    -1.0f,
       -0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,
//8
        0.0, 0.0f,  0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,
       -0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,
       -0.25f,0.0f,0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     -1.0f,//23

    //Tubo semicircular
            //x        y        z        S        T            NX        NY        NZ
        //1
            0.25f,0.0f,0.3f,           0.8f,    1.0f,        -1.0f,     0.0f,    0.0f,//24
            0.231695f, 0.106695f,0.3f, 0.8f,    1.0f,        -1.0f,    0.0f,    0.0f,
            0.25f,0.0f,-0.3f,           0.8f,    1.0f,        -1.0f,    0.0f,    0.0f,
            0.231695f, 0.106695f,-0.3f,0.8f,    1.0f,        -1.0f,    0.0f,    0.0f,
           //2
          0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        -1.0f,    0.0f,    0.0f,
          0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
          0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        -1.0f,    0.0f,    0.0f,
          0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
           //3
           0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
           0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
           0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
           0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        -1.0f,   0.0f,   0.0f,
           //4
           0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        -1.0f,   -1.0f,   0.0f,
           0.0f, 0.25f,0.3f,           0.8f, 1.0f,        -1.0f,    -1.0f,   0.0f,
           0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        -1.0f,   -1.0f,   0.0f,
           0.0f, 0.25f,-0.3f,           0.8f, 1.0f,        -1.0f,    -1.0f,   0.0f,

           //5
            0.0f, 0.25f,0.3f,           0.8f,  1.0f,        1.0f,    0.0f,   0.0f,
           -0.106695f, 0.231695f,0.3f, 0.8f, 1.0f,        1.0f,   0.0f,   0.0f,
            0.0f, 0.25f,-0.3f,           0.8f,  1.0f,        1.0f,    0.0f,   0.0f,
           -0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        1.0f,   0.0f,   0.0f,
           //6
            -0.106695f, 0.231695f,  0.3f, 0.8f, 1.0f, 1.0f, 0.0f, 0.0f,
            -0.182140f, 0.182140f,  0.3f, 0.8f, 1.0f, 1.0f, 0.0f, 0.0f,
            -0.106695f, 0.231695f, -0.3f, 0.8f, 1.0f, 1.0f, 0.0f, 0.0f,
            -0.182140f, 0.182140f, -0.3f, 0.8f, 1.0f, 1.0f, 0.0f, 0.0f,
           //7
           -0.182140f, 0.182140f,0.3f, 0.8f, 1.0f,        1.0f,   0.0f,   0.0f,
           -0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        1.0f,    0.0f,     0.0f,
           -0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        1.0f,   0.0f,   0.0f,
           -0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        1.0f,    0.0f,     0.0f,
           //8
           -0.231695f, 0.106695f,0.3f, 0.8f, 1.0f,        1.0f,    0.0f,     0.0f,
           -0.25f,0.0f,0.3f,           0.8f, 1.0f,        1.0f,    0.0f,     0.0f,
           -0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        1.0f,    0.0f,     0.0f,
           -0.25f,0.0f,-0.3f,           0.8f, 1.0f,        1.0f,    0.0f,     0.0f,//55
    //Atrás Semicircular
//1
               //x        y        z        S        T
        0.0, 0.0f,  -0.3f,          0.8f,    1.0f,        0.0f,    0.0f,     1.0f,//56
        0.25f,0.0f,-0.3f,          0.8f,    1.0f,        0.0f,    0.0f,     1.0f,
        0.231695f, 0.106695f,-0.3f,0.8f,    1.0f,        0.0f,    0.0f,     1.0f,
//2
        0.0, 0.0f, -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     1.0f,
        0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        0.0f,    0.0f,     1.0f,
        0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    1.0f,
//3
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     1.0f,
        0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    1.0f,
        0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    1.0f,
//4
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     1.0f,
        0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    1.0f,
        0.0f, 0.25f,-0.3f,           0.8f, 1.0f,        0.0f,    0.0f,    1.0f,
//5
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,     1.0f,
        0.0f, 0.25f,-0.3f,           0.8f,  1.0f,        0.0f,    0.0f,    1.0f,
       -0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,    1.0f,
//6
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,      1.0f,
       -0.106695f, 0.231695f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,     1.0f,
       -0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,     1.0f,
//7
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,      1.0f,
       -0.182140f, 0.182140f,-0.3f, 0.8f, 1.0f,        0.0f,   0.0f,     1.0f,
       -0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        0.0f,    0.0f,      1.0f,
//8
        0.0, 0.0f,  -0.3f,           0.8f, 1.0f,        0.0f,    0.0f,      1.0f,
       -0.231695f, 0.106695f,-0.3f, 0.8f, 1.0f,        0.0f,    0.0f,      1.0f,
       -0.25f,0.0f,-0.3f,           0.8f, 1.0f,        0.0f,    0.0f,      1.0f,      //79

    //Baño
        // front                                        Intertir las normales
        //x        y        z        S        T            NX        NY        NZ
        -0.25f,  -1.0f,  0.3f,    0.334f,    0.0f,        0.0f,    0.0f,    -1.0f, //80
         0.25f,  -1.0f,  0.3f,    0.665f,    0.0f,        0.0f,    0.0f,    -1.0f,
         0.25f,   0.0f,  0.3f,    0.665f,    0.79f,        0.0f,    0.0f,    -1.0f,
        -0.25f,   0.0f,  0.3f,    0.334f,    0.79f,        0.0f,    0.0f,    -1.0f,
        // right
        //x        y        z        S        T
        0.25f, -1.0f,  0.3f,    0.0f,    0.0f,        -1.0f,    0.0f,    0.0f,
        0.25f, -1.0f,  -0.3f,    0.332f,    0.0f,        -1.0f,    0.0f,    0.0f,
        0.25f,  0.0f,  -0.3f,    0.332f,    0.79f,        -1.0f,    0.0f,    0.0f,
        0.25f,  0.0f,  0.3f,    0.0f,    0.79f,        -1.0f,    0.0f,    0.0f,
        // back
        -0.25f, -1.0f, -0.3f,    0.667f,    0.0f,        0.0f,    0.0f,    1.0f,
        0.25f, -1.0f, -0.3f,    1.0f,    0.0f,        0.0f,    0.0f,    1.0f,
        0.25f,  0.0f, -0.3f,    1.0f,    0.79f,        0.0f,    0.0f,    1.0f,
        -0.25f,  0.0f, -0.3f,    0.667f,    0.79f,        0.0f,    0.0f,    1.0f,

        // left
        //x        y        z        S        T
        -0.25f, -1.0f,  -0.3f,    0.0f,    0.0f,        1.0f,    0.0f,    0.0f,
        -0.25f, -1.0f,  0.3f,    0.332f,    0.0f,        1.0f,    0.0f,    0.0f,
        -0.25f,  0.0f,  0.3f,    0.332f,    0.79f,        1.0f,    0.0f,    0.0f,
        -0.25f,  0.0f,  -0.3f,    0.0f,    0.79f,        1.0f,    0.0f,    0.0f,

        // bottom                                    //SI van las normales correctas (sin invertir)
        //x        y        z        S        T
       -0.25f, -1.0f,  0.3f,    0.8f,    1.0f,        0.0f,    -1.0f,    0.0f,//y negativo
        0.25f,  -1.0f,  0.3f,    0.8f,    1.0f,        0.0f,    -1.0f,    0.0f,
        0.25f,  -1.0f,  -0.3f,    0.8f,    1.0f,        0.0f,    -1.0f,    0.0f,
       -0.25f, -1.0f,  -0.3f,    0.8f,    1.0f,        0.0f,    -1.0f,    0.0f //99

    };
    //calcAverageNormals(cabeza_indices, 36, cabeza_vertices, 192, 8, 5);
    Mesh *bano = new Mesh();
    bano->CreateMesh(bano_vertices, bano_indices, 800, 126); //Todo es correcto
    meshList.push_back(bano);

}
void loadTextures()
{
    camino_T = Texture("Textures/camino.tga");
    camino_T.LoadTextureA();
    acera_T = Texture("Textures/acera.tga");
    acera_T.LoadTextureA();
    calle_T = Texture("Textures/calle.tga");
    calle_T.LoadTextureA();
    pasto_T = Texture("Textures/pasto.tga");
    pasto_T.LoadTextureA();
    esquina_calle_T = Texture("Textures/esquina_calle.tga");
    esquina_calle_T.LoadTextureA();
    loceta_T = Texture("Textures/loceta.tga");
    loceta_T.LoadTextureA();
    bote_T = Texture("Textures/bote.tga");
    bote_T.LoadTextureA();
    T_super = Texture("Textures/superman.tga");
    T_super.LoadTextureA();
    T_slumpy = Texture("Textures/pielAzul.tga");
    T_slumpy.LoadTextureA();
    T_slumpyF = Texture("Textures/pielAzulF.tga");
    T_slumpyF.LoadTextureA();
    faceTexture = Texture("Textures/face.tga");
    faceTexture.LoadTextureA();
    caraLumpy = Texture("Textures/caraLumpy.tga");
    caraLumpy.LoadTextureA();
    pielTexture = Texture("Textures/piel.tga");
    pielTexture.LoadTextureA();
    T_rojo = Texture("Textures/rojo.tga");
    T_rojo.LoadTextureA();
    T_cuernos = Texture("Textures/colorCuerno.tga");
    T_cuernos.LoadTextureA();
    T_cuernosF = Texture("Textures/colorCuernoF.tga");
    T_cuernosF.LoadTextureA();
    roundBrickTexture = Texture("Textures/roundbrick.tga");
    roundBrickTexture.LoadTextureA();
    brick241Texture = Texture("Textures/brick241.tga");
    brick241Texture.LoadTextureA();
    brick223Texture = Texture("Textures/brick223.tga");
    brick223Texture.LoadTextureA();
    faro_T = Texture("Textures/faro.tga");
    faro_T.LoadTextureA();
    banoHTexture = Texture("Textures/BanoH.tga");
    banoHTexture.LoadTexture();
    banoMTexture = Texture("Textures/BanoM.tga");
    banoMTexture.LoadTexture();
    flash_T = Texture("Textures/flash.tga");
    flash_T.LoadTextureA();
    lex_T = Texture("Textures/lex.tga");
    lex_T.LoadTextureA();
    facesorp_T = Texture("Textures/face_sorp.tga");
    facesorp_T.LoadTextureA();
    onomatopeya_T = Texture("Textures/onomatopeya.tga");
    onomatopeya_T.LoadTextureA();
    cabina_T = Texture("Textures/Cabina2.tga");
    cabina_T.LoadTextureA();
    puerta_T = Texture("Textures/Puerta.tga");
    puerta_T.LoadTextureA();
    agua_T = Texture("Textures/agua.tga");
    agua_T.LoadTextureA();
    caraLumpyF = Texture("Textures/caraLumpyF.tga");
    caraLumpyF.LoadTextureA();
    caraLumpyM = Texture("Textures/caraLumpyM.tga");
    caraLumpyM.LoadTextureA();

}
void loadModels()
{
    arbusto_M= Model();
    arbusto_M.LoadModel("Models/arbusto.obj");
    arbol_M= Model();
    arbol_M.LoadModel("Models/arbol.obj");
    reja_M= Model();
    reja_M.LoadModel("Models/rejas.obj");
    llanta_M= Model();
    llanta_M.LoadModel("Models/llanta.obj");
    auto_M= Model();
    auto_M.LoadModel("Models/carro.obj");
    decaedro_M = Model();
    decaedro_M.LoadModel("Models/decaedroColor.obj");
    Blackhawk_M = Model();
    Blackhawk_M.LoadModel("Models/uh60.obj");
    fuente_M = Model();
    fuente_M.LoadModel("Models/fuente.obj");
    lantern_M = Model();
    lantern_M.LoadModel("Models/streetlamp.obj");
    wanda_M = Model();
    wanda_M.LoadModel("Models/wanda.obj");
    cosmo_M = Model();
    cosmo_M.LoadModel("Models/cosmo.obj");
    pecera_M = Model();
    pecera_M.LoadModel("Models/pecera_elements.obj");
    halo_M = Model();
    halo_M.LoadModel("Models/halo.obj");

}
void CreateShaders()
{
    Shader *shader1 = new Shader();
    shader1->CreateFromFiles(vShader, fShader);
    shaderList.push_back(*shader1);
}
void unirPiso();
void UnirKiosco();
void UnirSuperman(float posX, float posY, float posZ);
void UnirLumpy();
void UnirLumpyFantasma();
void colocarArbustos();
void colocarArboles();
void colocarRejas();
void colocarObjetos();
void colocarAuto();
void RenderBlackhawk();
void colocarCabina();
void colocarPeces();

void animarAuto();

void espectaculoLuces(int pointLightCount, int spotLightCount, bool encendido, GLfloat tiempo);

//funciones keyframes
void resetElements();
void resetElements2();
void resetElements3();
void resetElements4();
void interpolation();
void interpolation2();
void interpolation3();
void interpolation4();
void animate();
void animate2();
void animate3();
void animate4();
void leerKeyFrames();

int main()
{
    SDL_Init(SDL_INIT_AUDIO);
    //SDL_AudioSpec wavSpec;
    sound.load("superman.wav");
    effect.load("helicopter.wav");
    wind.load("Explosion.wav");
    repito.load("Repito.wav");
    
    bool encendido;
    bool avanza = false;
    GLfloat tiempo = 0.0f;
    mainWindow = Window(1366, 768); // 1280, 1024 or 1024, 768
    mainWindow.Initialise();

    CrearPiso(); //0
    crearSuperman(); //1 - 8
    crearLumpy(); //9
    crearBloquesKiosco(); //10 - 15
    CrearCalle(); //15
    CrearEsquina(); //17 - 19
    CrearObjetos(); //20 - 21
    CrearBano(); //22
    crearPlano(); //23 - 26
    CreateBoomerang(); //27
    CreateWings(); //28
    CreateShaders();

    camera = Camera(glm::vec3(0.0f, 1.50f, 25.0f), glm::vec3(0.0f, 1.0f, 0.0f), -60.0f, 0.0f, 5.0f, 0.5f);

    loadTextures();
    
    Material_brillante = Material(4.0f, 256);
    Material_opaco = Material(0.3f, 4);
    
    loadModels();
    
    Skybox skybox;
    
    //luz direccional, sÛlo 1 y siempre debe de existir
    mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
                                0.75f, 0.75f,
                                0.0f, -1.0f, -1.0f);

    //luces de tipo pointlight
    unsigned int pointLightCount = 0;

    //luces spotlight
    unsigned int spotLightCount = 0;
    
    std::vector<std::string> skyboxFaces;
    
    skyboxFaces.push_back("Textures/Skybox/xpos.png");
    skyboxFaces.push_back("Textures/Skybox/xneg.png");
    skyboxFaces.push_back("Textures/Skybox/yneg.png");
    skyboxFaces.push_back("Textures/Skybox/ypos.png");
    skyboxFaces.push_back("Textures/Skybox/zpos.png");
    skyboxFaces.push_back("Textures/Skybox/zneg.png");

    skybox = Skybox(skyboxFaces);

    GLuint uniformProjection = 0, uniformView = 0, uniformEyePosition = 0,
        uniformSpecularIntensity = 0, uniformShininess = 0;
    glm::mat4 projection = glm::perspective(45.0f, (GLfloat)mainWindow.getBufferWidth() / mainWindow.getBufferHeight(), 0.1f, 100.0f);
    
    //FUNCIÓN PARA LEER LOS KEYFRAMES
    leerKeyFrames();

    //sound.play();
    
    sp.init();
    sp.load();
    
    //Loop mientras no se cierra la ventana
    while (!mainWindow.getShouldClose())
    {
        movLlanta += 3.0f;
        encendido = mainWindow.getOn();
        
        //if(!sound.getBitsRemaining()) sound.play();
        
        GLfloat now = glfwGetTime();
        deltaTime = now - lastTime;
        lastTime = now;
        tiempo += deltaTime;

        //Recibir eventos del usuario
        glfwPollEvents();

        camera.keyControl(mainWindow.getsKeys(), deltaTime);
        camera.mouseControl(mainWindow.getXChange(), mainWindow.getYChange());
        
        //para keyframes
        inputKeyframes(mainWindow.getsKeys());
        animate();
        animate2();
        animate3();
        animate4();

        // Clear the window
        glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        skybox.DrawSkybox(camera.calculateViewMatrix(), projection);

        shaderList[0].UseShader();
        uniformModel = shaderList[0].GetModelLocation();
        uniformProjection = shaderList[0].GetProjectionLocation();
        uniformView = shaderList[0].GetViewLocation();
        uniformEyePosition = shaderList[0].GetEyePositionLocation();
        uniformSpecularIntensity = shaderList[0].GetSpecularIntensityLocation();
        uniformShininess = shaderList[0].GetShininessLocation();

        shaderList[0].SetPointLights(pointLights, pointLightCount);
        shaderList[0].SetSpotLights(spotLights, spotLightCount);
        shaderList[0].SetDirectionalLight(&mainLight);

        //Apagamos/Encendemos Luz principal
        if (!avanza)
        {
            mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
                0.75f, 0.75f,
                0.0f, -1.0f, -1.0f);
        }
        else
        {
            mainLight = DirectionalLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 0.0f, 0.0f);
            espectaculoLuces(pointLightCount, spotLightCount, encendido, tiempo);
        }

        glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera.calculateViewMatrix()));
        glUniform3f(uniformEyePosition, camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);

        //suelo
        unirPiso();
        
        /* Instanciar kiosco*/
        UnirKiosco();
        
        /* Instanciar arbustos*/
        colocarArbustos();
        
        /* Instanciar árboles*/
        colocarArboles();
        
        /* Instanciar rejas*/
        colocarRejas();
        
        /* Instancia Lumpy*/
        UnirLumpy();
        
        /*Animación sencilla del auto*/
        animarAuto();
        
        /*Animación simple superman*/
        switch (animar) {
        case 0:
            if (giroSy < 12000.0f) {
                recorrido += 50.0f;
                giroSy = pow((0.002 * recorrido), 2);
                //printf("\nGIRO:%f", giroSy);
            }
            else {
                giroSy = 0.0f;
                recorrido = 0.0f;
                animar = 1;
                tiempo_transform = 0.0f;
                ropa++;
            }
            break;
        case 1:
            if (tiempo_transform < 5.0f) tiempo_transform += deltaTime;
            else if (ropa < 2)
                 animar = 0;
            else {
                animar = 3;
                play2 = true;
                finAnimacion = true;
            }
            break;
        }
        /*Animación simple Lumpy*/
          
        if (activaLF) {
            if (subida <= 1000.0f) {
                UnirLumpyFantasma();
                velocidadLF += 3.0f;
                giroLF += 0.4f;
                subida = subida + deltaTime;
                }
            }
        
        //ANIMACIóN COMPLEJA BOOMERANG
        if (phi < 6.2832f and soyBatman) {
                precalculo = pow(cos(phi), 2) / 400 + pow(sin(phi), 2) / 100;
                r = 1 / sqrt(precalculo);
                phi += thetaOffset2;
                posxB = r * cos(phi);
                poszB = r * sin(phi);
                giroB += thetaOffset2;
               } else if (soyBatman) {
                   soyBatman = false;
                   phi = 0.0f;
                   giroB = 0.0f;
               }
                   
        /*Instanciar auto*/
        colocarAuto();
        
        /*Instanciar faros y basureros*/
        colocarObjetos();
        
        if(tiempo > 120.0f and !avanza) {
            avanza = true;
            tiempo = 0.0f;
            pointLightCount = 10;
            spotLightCount = 6;
            skyboxFaces[0] = "Textures/Skybox/corona_ft.png";
            skyboxFaces[1] = "Textures/Skybox/corona_bk.png";
            skyboxFaces[2] = "Textures/Skybox/corona_dn.png";
            skyboxFaces[3] = "Textures/Skybox/corona_up.png";
            skyboxFaces[4] = "Textures/Skybox/corona_rt.png";
            skyboxFaces[5] = "Textures/Skybox/corona_lf.png";
            skybox = Skybox(skyboxFaces);
            printf("\nENTRA NOCHE: %f",tiempo);
        } else if(tiempo > 120.0f and avanza) {
            avanza = false;
            tiempo = 0.0f;
            pointLightCount = 0;
            spotLightCount = 0;
            skyboxFaces[0] = "Textures/Skybox/xpos.png";
            skyboxFaces[1] = "Textures/Skybox/xneg.png";
            skyboxFaces[2] = "Textures/Skybox/yneg.png";
            skyboxFaces[3] = "Textures/Skybox/ypos.png";
            skyboxFaces[4] = "Textures/Skybox/zpos.png";
            skyboxFaces[5] = "Textures/Skybox/zneg.png";
           // lucesDia(pointLightCount, spotLightCount);
            skybox = Skybox(skyboxFaces);
            printf("\nENTRA DIA: %f",tiempo);
        }
        
        /*Animación compleja de superman volando*/
        if(caer and posY < 30.0f) {
            posX = curvatura * cos(theta) + 3.0f;
            posZ = curvatura * sin(theta) - 20.55f;
            theta += thetaOffset;
            curvatura = 2.0f * theta;
            posY += 0.05f;
            giroSy += 0.5f;
            if(posY > 29.96f) wind.play();
        } else if(caer and posY < 200.0f) {
            giroSy = 0.0f;
            posY += 0.5f;
            posZ += 0.5f;
        }
        
        /*Renderizar Helicoptero*/
        RenderBlackhawk();
        
        /* Instancia superman*/
        UnirSuperman(posX, posY, posZ);
        
        /*Renderizar cabina*/
        colocarCabina();
        
        /*Colocar pecera, cosmo y wanda*/
        colocarPeces();
        
        /*Animación compleja cosmo*/
        switch (flotar) {
            case 1:
                if(posCosmoY < 2.2f) {
                    posCosmoX = radio * cos(epsilon) + 25.0f;
                    posCosmoZ = radio * sin(epsilon);
                    epsilon += 0.02f;
                    radio = 1.4f / epsilon;
                    posCosmoY += 0.005f;
                    giroCosmoY += 0.5f;
                } else {
                    flotar = 2;
                    epsilon = 0.0f;
                    radio = 0.258f;
                }
                break;
            case 2:
                if(posCosmoY > 1.0f) {
                    posCosmoX = radio * cos(epsilon) + 25.0f;
                    posCosmoZ = radio * sin(epsilon);
                    epsilon += 0.02f;
                    radio = 0.258f * epsilon;
                    posCosmoY -= 0.005f;
                    giroCosmoY += 0.5f;
                }else if(posCosmoZ < 1.1f) {
                    posCosmoX+= 0.006f;
                    posCosmoZ = 3.602 * posCosmoX -91.67f;
                } else {
                    flotar = 3;
                    banderaSound = true;
                }
                break;
            default:
                break;
        }
        
        if(flotar == 3 and banderaCosmo) {
            if(giroCosmoY < 250.5f) giroCosmoY+= 0.5f;
            else banderaCosmo = false;
        } else if(flotar == 3 and !banderaCosmo) {
            if(giroCosmoY > 240.5f) giroCosmoY-= 0.5f;
            else banderaCosmo = true;
        }
        
        if(!play4 and banderaCosmo) {
            if(giroYW < 370.0f) giroYW+= 0.5f;
        } else if(!play4 and !banderaCosmo) {
            if(giroYW > 360.0f) giroYW-= 0.5f;
        }
        
        glUseProgram(0);

        mainWindow.swapBuffers();
    }
    SDL_Quit();

    return 0;
}

void colocarPeces()
{
    glm::mat4 model(1.0);
    //cosmo
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(posCosmoX, posCosmoY, posCosmoZ));
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
    model = glm::rotate(model, giroCosmoY * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    cosmo_M.RenderModel();
    
    //wanda
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(posXW + movXW, posYW +movYW, posZW + movZW));
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
    model = glm::rotate(model, giroYW * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    wanda_M.RenderModel();
    //castillo
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(25.0f, 0.3f, -0.8f));
    //model = glm::translate(model, glm::vec3(0.0f, 0.0f, -4.0f));
    model = glm::scale(model, glm::vec3(0.15f, 0.12f, 0.15f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    pecera_M.RenderModel();
    
    //pecera
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(25.0f, 1.0f, 0.0f));
    //model = glm::translate(model, glm::vec3(0.0f, 0.0f, -4.0f));
    model = glm::scale(model, glm::vec3(0.08f, 0.08f, 0.08f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    agua_T.UseTexture();
    sp.render();
    glDisable(GL_BLEND);
}

void inputKeyframes(bool* keys) {
    if (keys[GLFW_KEY_SPACE]) //Iniciar animación
    {
        if(reproduciranimacion<1)
        {
            if (play == false && (FrameIndex > 1))
            {
                resetElements();
                //First Interpolation
                interpolation();
                play = true;
                playIndex = 0;
                i_curr_steps = 0;
                effect.play();
                reproduciranimacion++;
                printf("presiona 0 para habilitar reproducir de nuevo la animación'\n");
                habilitaranimacion = 0;

            }
            else play = false;
        }
    }
    if (keys[GLFW_KEY_0]) //Resetear animación
    {
        if (habilitaranimacion < 1) reproduciranimacion = 0;
    }
    //Animacion superman
    if (keys[GLFW_KEY_5]) //Inicia animaciÛn 2
    {
        if (reproduciranimacion2 < 1)
        {
            if (play2 == false && (FrameIndex2 > 1))
            {
                resetElements2();
                //First Interpolation
                interpolation2();
                play2 = true;
                playIndex2 = 0;
                i_curr_steps_2 = 0;
                reproduciranimacion2++;
                printf("presiona 1 para habilitar reproducir de nuevo la animación'\n");
                habilitaranimacion2 = 0;

            }
            else play2 = false;
        }
    }

    if (keys[GLFW_KEY_1]) //Resetear animación Superman
    {
        if (habilitaranimacion2 < 1) reproduciranimacion2 = 0;
    }
    
    //AnimaciÛn de Lumpy
    if (keys[GLFW_KEY_6]) //Inicia animaciÛn 3
    {
        if (reproduciranimacion3 < 1)
        {
            if (play3 == false && (FrameIndex3 > 1))
            {
                resetElements3();
                //First Interpolation
                interpolation3();
                play3 = true;
                playIndex3 = 0;
                i_curr_steps_3 = 0;
                reproduciranimacion3++;
                printf("presiona 1 para habilitar reproducir de nuevo la animación'\n");
                habilitaranimacion3 = 0;

            }
            else play3 = false;
        }
    }

    if (keys[GLFW_KEY_2]) //Resetear animación Lumpy
    {
        if (habilitaranimacion3 < 1) reproduciranimacion3 = 0;
    }
    
    //AnimaciÛn de Wanda
    if (keys[GLFW_KEY_7]) //Inicia animaciÛn 3
    {
        flotar = 1;
        banderaCosmo = true;
        if (reproduciranimacion4 < 1)
        {
            if (play4 == false && (FrameIndex4 > 1))
            {
                resetElements4();
                //First Interpolation
                interpolation4();
                play4 = true;
                playIndex4 = 0;
                i_curr_steps_4 = 0;
                reproduciranimacion4++;
                printf("presiona 1 para habilitar reproducir de nuevo la animación'\n");
                habilitaranimacion4 = 0;

            }
            else play4 = false;
        }
    }

    if (keys[GLFW_KEY_3]) //Resetear animación Cosmo y Wanda
    {
        if (flotar == 3 and banderaSound)
        {
            banderaSound = false;
            repito.play();
        }
        if (habilitaranimacion4 < 1) {
            reproduciranimacion4 = 0;
        }
    }
}

void resetElements(void) //cuando se reproduce de nuevo la animación, se resetean las variables
{
    
    movAvion_x = KeyFrame[0].movAvion_x;
    movAvion_y = KeyFrame[0].movAvion_y;
    movAvion_z = KeyFrame[0].movAvion_z;
    giroAvion= KeyFrame[0].giroAvion;
}

//Para Superman
void resetElements2(void) //cuando se reproduce de nuevo la animación, se resetean las variables
{
    posX = 3.0f, posY = 0.55f, posZ = -20.55f;
    caer = false;
    curvatura = 0.0f;
    theta = 0.0f;
    ropa = 0;
    giroSy = 0.0f;
    finAnimacion = false;
    movSup_x = KeyFrame2[0].movSup_x;
    movSup_z = KeyFrame2[0].movSup_z;
    giroSup = KeyFrame2[0].giroSup;
    giroPIzq = KeyFrame2[0].giroPIzq;
    giroPDer = KeyFrame2[0].giroPDer;
    giroBIzq = KeyFrame2[0].giroBIzq;
    giroBDer = KeyFrame2[0].giroBDer;
    puerta = KeyFrame2[0].puerta;
}

//Para Lumpy
void resetElements3(void) //cuando se reproduce de nuevo la animación, se resetean las variables
{
    posXL = 20.0f, posYL = 0.55f, posZL = -3.0f;
    boomerangFijo = false;
    boomerangMano = true;
    activaLF = false;
    subida = 0.0f;
    giroLF = 0.0f;
    movLum_x = KeyFrame3[0].movLum_x;
    movLum_y = KeyFrame3[0].movLum_y;
    movLum_z = KeyFrame3[0].movLum_z;
    giroLumY = KeyFrame3[0].giroLumY;
    giroLumX = KeyFrame3[0].giroLumX;
    giroLumZ = KeyFrame3[0].giroLumZ;
    giroPIzqL = KeyFrame3[0].giroPIzqL;
    giroPDerL = KeyFrame3[0].giroPDerL;
    giroBIzqL = KeyFrame3[0].giroBIzqL;
    giroBDerL = KeyFrame3[0].giroBDerL;
}

//Para Wanda
void resetElements4(void) //cuando se reproduce de nuevo la animación, se resetean las variables
{
    movXW = KeyFrame4[0].movXW;
    movYW = KeyFrame4[0].movYW;
    movZW = KeyFrame4[0].movZW;
    giroYW = KeyFrame4[0].giroYW;
    epsilon = 1.0f;
    posCosmoX = 25.756f;
    posCosmoZ = 1.1f;
    posCosmoY = 1.0f;
    radio = 1.4f;
    giroCosmoY = 0.0f;
    banderaCosmo = false;

}
    
void interpolation(void) //se calculan los incrementos
{
    KeyFrame[playIndex].movAvion_xInc = (KeyFrame[playIndex + 1].movAvion_x - KeyFrame[playIndex].movAvion_x) / i_max_steps;
    KeyFrame[playIndex].movAvion_yInc = (KeyFrame[playIndex + 1].movAvion_y - KeyFrame[playIndex].movAvion_y) / i_max_steps;
    KeyFrame[playIndex].movAvion_zInc = (KeyFrame[playIndex + 1].movAvion_z - KeyFrame[playIndex].movAvion_z) / i_max_steps;
    KeyFrame[playIndex].giroAvionInc = (KeyFrame[playIndex + 1].giroAvion - KeyFrame[playIndex].giroAvion) / i_max_steps;
    
}

void interpolation2(void) //se calculan los incrementos
{
    KeyFrame2[playIndex2].movSup_xInc = (KeyFrame2[playIndex2 + 1].movSup_x - KeyFrame2[playIndex2].movSup_x) / i_max_steps_2;
    KeyFrame2[playIndex2].movSup_zInc = (KeyFrame2[playIndex2 + 1].movSup_z - KeyFrame2[playIndex2].movSup_z) / i_max_steps_2;
    KeyFrame2[playIndex2].giroSupInc = (KeyFrame2[playIndex2 + 1].giroSup - KeyFrame2[playIndex2].giroSup) / i_max_steps_2;
    KeyFrame2[playIndex2].giroPIzqInc = (KeyFrame2[playIndex2 + 1].giroPIzq - KeyFrame2[playIndex2].giroPIzq) / i_max_steps_2;
    KeyFrame2[playIndex2].giroPDerInc = (KeyFrame2[playIndex2 + 1].giroPDer - KeyFrame2[playIndex2].giroPDer) / i_max_steps_2;
    KeyFrame2[playIndex2].giroBIzqInc = (KeyFrame2[playIndex2 + 1].giroBIzq - KeyFrame2[playIndex2].giroBIzq) / i_max_steps_2;
    KeyFrame2[playIndex2].giroBDerInc = (KeyFrame2[playIndex2 + 1].giroBDer - KeyFrame2[playIndex2].giroBDer) / i_max_steps_2;
    KeyFrame2[playIndex2].puertaInc = (KeyFrame2[playIndex2 + 1].puerta - KeyFrame2[playIndex2].puerta) / i_max_steps_2;
}
    
void interpolation3(void) //se calculan los incrementos
{
    KeyFrame3[playIndex3].movLum_xInc = (KeyFrame3[playIndex3 + 1].movLum_x - KeyFrame3[playIndex3].movLum_x) / i_max_steps_3;
    KeyFrame3[playIndex3].movLum_yInc = (KeyFrame3[playIndex3 + 1].movLum_y - KeyFrame3[playIndex3].movLum_y) / i_max_steps_3;
    KeyFrame3[playIndex3].movLum_zInc = (KeyFrame3[playIndex3 + 1].movLum_z - KeyFrame3[playIndex3].movLum_z) / i_max_steps_3;
    KeyFrame3[playIndex3].giroLumXInc = (KeyFrame3[playIndex3 + 1].giroLumX - KeyFrame3[playIndex3].giroLumX) / i_max_steps_3;
    KeyFrame3[playIndex3].giroLumYInc = (KeyFrame3[playIndex3 + 1].giroLumY - KeyFrame3[playIndex3].giroLumY) / i_max_steps_3;
    KeyFrame3[playIndex3].giroLumZInc = (KeyFrame3[playIndex3 + 1].giroLumZ - KeyFrame3[playIndex3].giroLumZ) / i_max_steps_3;
    KeyFrame3[playIndex3].giroPIzqLInc = (KeyFrame3[playIndex3 + 1].giroPIzqL - KeyFrame3[playIndex3].giroPIzqL) / i_max_steps_3;
    KeyFrame3[playIndex3].giroPDerLInc = (KeyFrame3[playIndex3 + 1].giroPDerL - KeyFrame3[playIndex3].giroPDerL) / i_max_steps_3;
    KeyFrame3[playIndex3].giroBIzqLInc = (KeyFrame3[playIndex3 + 1].giroBIzqL - KeyFrame3[playIndex3].giroBIzqL) / i_max_steps_3;
    KeyFrame3[playIndex3].giroBDerLInc = (KeyFrame3[playIndex3 + 1].giroBDerL - KeyFrame3[playIndex3].giroBDerL) / i_max_steps_3;
    }

void interpolation4(void) //se calculan los incrementos
{
    KeyFrame4[playIndex4].movXW_Inc = (KeyFrame4[playIndex4 + 1].movXW - KeyFrame4[playIndex4].movXW) / i_max_steps_4;
    KeyFrame4[playIndex4].movYW_Inc = (KeyFrame4[playIndex4 + 1].movYW - KeyFrame4[playIndex4].movYW) / i_max_steps_4;
    KeyFrame4[playIndex4].movZW_Inc = (KeyFrame4[playIndex4 + 1].movZW - KeyFrame4[playIndex4].movZW) / i_max_steps_4;
    KeyFrame4[playIndex4].giroYW_Inc = (KeyFrame4[playIndex4 + 1].giroYW - KeyFrame4[playIndex4].giroYW) / i_max_steps_4;
}


void animate()
{
    //Movimiento del objeto
    if (play)
    {
        if (i_curr_steps >= i_max_steps) //end of animation between frames?
        {
            playIndex++;
            printf("KeyFrame[%d].movAvion_x = %f\n", playIndex, movAvion_x);
            printf("KeyFrame[%d].movAvion_y = %f\n", playIndex, movAvion_y);
            printf("KeyFrame[%d].giroAvion = %f\n", playIndex, giroAvion);
            if (playIndex > FrameIndex - 2)    //end of total animation?
            {
                printf("Frame index= %d\n", FrameIndex);
                printf("termina anim\n");
                playIndex = 0;
                play = false;
            }
            else //Next frame interpolations
            {
                //printf("entro aquí\n"); //19:37
                i_curr_steps = 0; //Reset counter
                //Interpolation
                interpolation();
            }
        }
        else
        {
            //printf("se quedó aqui\n");
            //printf("max steps: %f", i_max_steps);
            //Draw animation
            movAvion_x += KeyFrame[playIndex].movAvion_xInc;
            movAvion_y += KeyFrame[playIndex].movAvion_yInc;
            movAvion_z += KeyFrame[playIndex].movAvion_zInc;
            giroAvion += KeyFrame[playIndex].giroAvionInc;
            i_curr_steps++;
        }

    }
}

void animate2()
{
    //Movimiento del objeto
    if (play2 && playIndex != 16)
    {
        if (i_curr_steps_2 >= i_max_steps_2) //end of animation between frames?
        {
            playIndex2++;
            printf("KeyFrame2[%d].movSup_x = %f\n", playIndex2, movSup_x);
            printf("KeyFrame2[%d].movSup_z = %f\n", playIndex2, movSup_z);
            printf("KeyFrame2[%d].giroSup = %f\n", playIndex2, giroSup);
            printf("KeyFrame2[%d].giroPIzq = %f\n", playIndex2, giroPIzq);
            printf("KeyFrame2[%d].giroPDer = %f\n", playIndex2, giroPDer);
            printf("KeyFrame2[%d].giroBIzq = %f\n", playIndex2, giroBIzq);
            printf("KeyFrame2[%d].giroBDer = %f\n", playIndex2, giroBDer);
            printf("KeyFrame2[%d].puerta = %f\n", playIndex2, puerta);
            if (playIndex2 > FrameIndex2 - 2)    //end of total animation?
            {
                printf("Frame2 index= %d\n", FrameIndex2);
                printf("termina anim\n");
                playIndex2 = 0;
                play2 = false;
                caer = true;
            }
            else //Next frame interpolations
            {
                //printf("entro aquí\n"); //19:37
                i_curr_steps_2 = 0; //Reset counter
                //Interpolation
                interpolation2();
            }
        }
        else
        {
            //printf("se quedó aqui\n");
            //printf("max steps: %f", i_max_steps);
            //Draw animation
            movSup_x += KeyFrame2[playIndex2].movSup_xInc;
            movSup_z += KeyFrame2[playIndex2].movSup_zInc;
            giroSup += KeyFrame2[playIndex2].giroSupInc;
            giroPIzq += KeyFrame2[playIndex2].giroPIzqInc;
            giroPDer += KeyFrame2[playIndex2].giroPDerInc;
            giroBIzq += KeyFrame2[playIndex2].giroBIzqInc;
            giroBDer += KeyFrame2[playIndex2].giroBDerInc;
            puerta += KeyFrame2[playIndex2].puertaInc;
            i_curr_steps_2++;
        }

    }

    if (play2 && playIndex2 == 16)
    {
        if(finAnimacion == false)
        {
            animar = 0;
            play2 = false;
        }
        else
        {
            if (i_curr_steps_2 >= i_max_steps_2) //end of animation between frames?
            {
                playIndex2++;
                printf("KeyFrame2[%d].movSup_x = %f\n", playIndex2, movSup_x);
                printf("KeyFrame2[%d].movSup_z = %f\n", playIndex2, movSup_z);
                printf("KeyFrame2[%d].giroSup = %f\n", playIndex2, giroSup);
                printf("KeyFrame2[%d].giroPIzq = %f\n", playIndex2, giroPIzq);
                printf("KeyFrame2[%d].giroPDer = %f\n", playIndex2, giroPDer);
                printf("KeyFrame2[%d].giroBIzq = %f\n", playIndex2, giroBIzq);
                printf("KeyFrame2[%d].giroBDer = %f\n", playIndex2, giroBDer);
                printf("KeyFrame2[%d].puerta = %f\n", playIndex2, puerta);
                if (playIndex2 > FrameIndex2 - 2)    //end of total animation?
                {
                    printf("Frame2 index= %d\n", FrameIndex2);
                    printf("termina anim\n");
                    playIndex2 = 0;
                    play2 = false;
                }
                else //Next frame interpolations
                {
                    //printf("entro aquí\n"); //19:37
                    i_curr_steps_2 = 0; //Reset counter
                    //Interpolation
                    interpolation2();
                }
            }
            else
            {
                //printf("se quedó aqui\n");
                //printf("max steps: %f", i_max_steps);
                //Draw animation
                movSup_x += KeyFrame2[playIndex2].movSup_xInc;
                movSup_z += KeyFrame2[playIndex2].movSup_zInc;
                giroSup += KeyFrame2[playIndex2].giroSupInc;
                giroPIzq += KeyFrame2[playIndex2].giroPIzqInc;
                giroPDer += KeyFrame2[playIndex2].giroPDerInc;
                giroBIzq += KeyFrame2[playIndex2].giroBIzqInc;
                giroBDer += KeyFrame2[playIndex2].giroBDerInc;
                puerta += KeyFrame2[playIndex2].puertaInc;
                i_curr_steps_2++;
            }
        }
    }

}

void animate3()
{
    //Movimiento del objeto
    if (play3)
    {
        if (playIndex3 < 12)
        {
            boomerangMano = true;
        }else {
            boomerangMano = false;
            if (playIndex3 == 12)
            {
                soyBatman = true;
            }
            if (playIndex3 == 15)
            {
                boomerangFijo = true;
            }
        }
        if (i_curr_steps_3 >= i_max_steps_3) //end of animation between frames?
        {
            playIndex3++;
            if (playIndex3 == 17)
            {
                activaLF = true;
            }
            printf("KeyFrame3[%d].movLum_x = %f\n", playIndex3, movLum_x);
            printf("KeyFrame3[%d].movLum_y = %f\n", playIndex3, movLum_y);
            printf("KeyFrame3[%d].movLum_z = %f\n", playIndex3, movLum_z);
            printf("KeyFrame3[%d].giroLumX = %f\n", playIndex3, giroLumX);
            printf("KeyFrame3[%d].giroLumY = %f\n", playIndex3, giroLumY);
            printf("KeyFrame3[%d].giroLumZ = %f\n", playIndex3, giroLumZ);
            printf("KeyFrame3[%d].giroPIzqL = %f\n", playIndex3, giroPIzqL);
            printf("KeyFrame3[%d].giroPDerL = %f\n", playIndex3, giroPDerL);
            printf("KeyFrame3[%d].giroBIzqL = %f\n", playIndex3, giroBIzqL);
            printf("KeyFrame3[%d].giroBDerL = %f\n", playIndex3, giroBDerL);
            if (playIndex3 > FrameIndex3 - 2)    //end of total animation?
            {
                printf("Frame3 index= %d\n", FrameIndex3);
                printf("termina anim\n");
                playIndex3 = 0;
                play3 = false;
            }
            else //Next frame interpolations
            {
                i_curr_steps_3 = 0; //Reset counter
                    //Interpolation
                    interpolation3();
                }
            }
            else
            {
                //Draw animation
                movLum_x += KeyFrame3[playIndex3].movLum_xInc;
                movLum_y += KeyFrame3[playIndex3].movLum_yInc;
                movLum_z += KeyFrame3[playIndex3].movLum_zInc;
                giroLumX += KeyFrame3[playIndex3].giroLumXInc;
                giroLumY += KeyFrame3[playIndex3].giroLumYInc;
                giroLumZ += KeyFrame3[playIndex3].giroLumZInc;
                giroPIzqL += KeyFrame3[playIndex3].giroPIzqLInc;
                giroPDerL += KeyFrame3[playIndex3].giroPDerLInc;
                giroBIzqL += KeyFrame3[playIndex3].giroBIzqLInc;
                giroBDerL += KeyFrame3[playIndex3].giroBDerLInc;
                i_curr_steps_3++;
            }
        }
    }

void animate4()
{
    //Movimiento del objeto
    if (play4)
    {
        if (i_curr_steps_4 >= i_max_steps_4) //end of animation between frames?
        {
            printf("KeyFrame[%d].movXW = %f\n", playIndex4, movXW);
            printf("KeyFrame[%d].movYW = %f\n", playIndex4, movYW);
            printf("KeyFrame[%d].movZW = %f\n", playIndex4, movZW);
            printf("KeyFrame[%d].giroYW = %f\n", playIndex4, giroYW);
            playIndex4++;
            if (playIndex4 > FrameIndex4 - 2)    //end of total animation?
            {
                printf("Frame index= %d\n", FrameIndex4);
                printf("termina anim\n");
                playIndex4 = 0;
                play4 = false;
            }
            else //Next frame interpolations
            {
                i_curr_steps_4 = 0; //Reset counter
                //Interpolation
                interpolation4();
            }
            
        }
        else
        {
            //Draw animation
            movXW += KeyFrame4[playIndex4].movXW_Inc;
            movYW += KeyFrame4[playIndex4].movYW_Inc;
            movZW += KeyFrame4[playIndex4].movZW_Inc;
            giroYW += KeyFrame4[playIndex4].giroYW_Inc;
            i_curr_steps_4++;
        }
    }
}

void leerKeyFrames(){
        //KEYFRAMES DECLARADOS INICIALES
    fp = fopen ("keyframes.txt","r");
    int i = 0;
    int posicion = 0;
    float dato;
    
    while (posicion < FrameIndex) {
        fscanf(fp, "%f", &dato);
        if (i > 3) {
            posicion++;
            i = 0;
        }
        switch (i) {
            case 0:
                KeyFrame[posicion].movAvion_x = dato;
                break;
            case 1:
                KeyFrame[posicion].movAvion_y = dato;
                break;
            case 2:
                KeyFrame[posicion].movAvion_z = dato;
                break;
            case 3:
                KeyFrame[posicion].giroAvion = dato;
        }
        i++;
        
    }
    fclose(fp);

    //FUNCIÓN PARA LEER LOS KEYFRAMES Superman
    fp = fopen ("keyframes_superman.txt","r");
    i = 0;
    posicion = 0;
    while (posicion < FrameIndex2) {
        fscanf(fp, "%f", &dato);
        if (i > 7) {
            posicion++;
            i = 0;
        }
        switch (i) {
            case 0:
                KeyFrame2[posicion].movSup_x = dato;
                break;
            case 1:
                KeyFrame2[posicion].movSup_z = dato;
                break;
            case 2:
                KeyFrame2[posicion].giroSup = dato;
                break;
            case 3:
                KeyFrame2[posicion].giroPIzq = dato;
                break;
            case 4:
                KeyFrame2[posicion].giroPDer = dato;
                break;
            case 5:
                KeyFrame2[posicion].giroBIzq = dato;
                break;
            case 6:
                KeyFrame2[posicion].giroBDer = dato;
                break;
            case 7:
                KeyFrame2[posicion].puerta = dato;
                
        }
        i++;
        
    }
    fclose(fp);
    
    //FUNCIÓN PARA LEER LOS KEYFRAMES Lumpy
    //leerKeyFrames();

    KeyFrame3[0].movLum_x = 0.0f;
    KeyFrame3[0].movLum_y = 0.0f;
    KeyFrame3[0].movLum_z = 0.0f;
    KeyFrame3[0].giroLumY = 0.0f;
    KeyFrame3[0].giroLumX = 90.0f;
    KeyFrame3[0].giroLumZ = 0.0f;
    KeyFrame3[0].giroPIzqL = 0.0f;
    KeyFrame3[0].giroPDerL = 0.0f;
    KeyFrame3[0].giroBIzqL = 0.0f;
    KeyFrame3[0].giroBDerL = -90.0f;

    KeyFrame3[1].movLum_x = 0.0f;
    KeyFrame3[1].movLum_y = 0.0f;
    KeyFrame3[1].movLum_z = 0.25f;
    KeyFrame3[1].giroLumY = 0.0f;
    KeyFrame3[1].giroLumX = 82.5f;
    KeyFrame3[1].giroLumZ = 0.0f;
    KeyFrame3[1].giroPIzqL = 0.0f;
    KeyFrame3[1].giroPDerL = 45.0f;
    KeyFrame3[1].giroBIzqL = 8.0f;
    KeyFrame3[1].giroBDerL = -89.0f;

    KeyFrame3[2].movLum_x = 0.0f;
    KeyFrame3[2].movLum_y = 0.0f;
    KeyFrame3[2].movLum_z = 0.5f;
    KeyFrame3[2].giroLumY = 0.0f;
    KeyFrame3[2].giroLumX = 75.0f;
    KeyFrame3[2].giroLumZ = 0.0f;
    KeyFrame3[2].giroPIzqL = 45.0f;
    KeyFrame3[2].giroPDerL = 0.0f;
    KeyFrame3[2].giroBIzqL = -12.0f;
    KeyFrame3[2].giroBDerL = -88.0f;

    KeyFrame3[3].movLum_x = 0.0f;
    KeyFrame3[3].movLum_y = 0.0f;
    KeyFrame3[3].movLum_z = 0.75f;
    KeyFrame3[3].giroLumY = 0.0f;
    KeyFrame3[3].giroLumX = 67.5f;
    KeyFrame3[3].giroLumZ = 0.0f;
    KeyFrame3[3].giroPIzqL = 0.0f;
    KeyFrame3[3].giroPDerL = 45.0f;
    KeyFrame3[3].giroBIzqL = 8.0f;
    KeyFrame3[3].giroBDerL = -87.0f;

    KeyFrame3[4].movLum_x = 0.0f;
    KeyFrame3[4].movLum_y = 0.0f;
    KeyFrame3[4].movLum_z = 1.0f;
    KeyFrame3[4].giroLumY = 0.0f;
    KeyFrame3[4].giroLumX = 60.0f;
    KeyFrame3[4].giroLumZ = 0.0f;
    KeyFrame3[4].giroPIzqL = 45.0f;
    KeyFrame3[4].giroPDerL = 0.0f;
    KeyFrame3[4].giroBIzqL = -12.0f;
    KeyFrame3[4].giroBDerL = -86.0f;

    KeyFrame3[5].movLum_x = 0.0f;
    KeyFrame3[5].movLum_y = 0.0f;
    KeyFrame3[5].movLum_z = 1.25f;
    KeyFrame3[5].giroLumY = 0.0f;
    KeyFrame3[5].giroLumX = 52.5f;
    KeyFrame3[5].giroLumZ = 0.0f;
    KeyFrame3[5].giroPIzqL = 0.0f;
    KeyFrame3[5].giroPDerL = 45.0f;
    KeyFrame3[5].giroBIzqL = 8.0f;
    KeyFrame3[5].giroBDerL = -85.0f;

    KeyFrame3[6].movLum_x = 0.0f;
    KeyFrame3[6].movLum_y = 0.0f;
    KeyFrame3[6].movLum_z = 1.5f;
    KeyFrame3[6].giroLumY = 0.0f;
    KeyFrame3[6].giroLumX = 45.0f;
    KeyFrame3[6].giroLumZ = 0.0f;
    KeyFrame3[6].giroPIzqL = 45.0f;
    KeyFrame3[6].giroPDerL = 0.0f;
    KeyFrame3[6].giroBIzqL = -12.0f;
    KeyFrame3[6].giroBDerL = -84.0f;

    KeyFrame3[7].movLum_x = 0.0f;
    KeyFrame3[7].movLum_y = 0.0f;
    KeyFrame3[7].movLum_z = 1.75f;
    KeyFrame3[7].giroLumY = 0.0f;
    KeyFrame3[7].giroLumX = 37.5f;
    KeyFrame3[7].giroLumZ = 0.0f;
    KeyFrame3[7].giroPIzqL = 0.0f;
    KeyFrame3[7].giroPDerL = 45.0f;
    KeyFrame3[7].giroBIzqL = 8.0f;
    KeyFrame3[7].giroBDerL = -83.0f;

    KeyFrame3[8].movLum_x = 0.0f;
    KeyFrame3[8].movLum_y = 0.0f;
    KeyFrame3[8].movLum_z = 2.0f;
    KeyFrame3[8].giroLumY = 0.0f;
    KeyFrame3[8].giroLumX = 30.0f;
    KeyFrame3[8].giroLumZ = 0.0f;
    KeyFrame3[8].giroPIzqL = 45.0f;
    KeyFrame3[8].giroPDerL = 0.0f;
    KeyFrame3[8].giroBIzqL = -12.0f;
    KeyFrame3[8].giroBDerL = -82.0f;

    KeyFrame3[9].movLum_x = 0.0f;
    KeyFrame3[9].movLum_y = 0.0f;
    KeyFrame3[9].movLum_z = 2.25f;
    KeyFrame3[9].giroLumY = 0.0f;
    KeyFrame3[9].giroLumX = 22.5f;
    KeyFrame3[9].giroLumZ = 0.0f;
    KeyFrame3[9].giroPIzqL = 0.0f;
    KeyFrame3[9].giroPDerL = 45.0f;
    KeyFrame3[9].giroBIzqL = 8.0f;
    KeyFrame3[9].giroBDerL = -81.0f;

    KeyFrame3[10].movLum_x = 0.0f;
    KeyFrame3[10].movLum_y = 0.0f;
    KeyFrame3[10].movLum_z = 2.5f;
    KeyFrame3[10].giroLumY = 0.0f;
    KeyFrame3[10].giroLumX = 15.0f;
    KeyFrame3[10].giroLumZ = 0.0f;
    KeyFrame3[10].giroPIzqL = 45.0f;
    KeyFrame3[10].giroPDerL = 0.0f;
    KeyFrame3[10].giroBIzqL = -12.0f;
    KeyFrame3[10].giroBDerL = -80.0f;

    KeyFrame3[11].movLum_x = 0.0f;
    KeyFrame3[11].movLum_y = 0.0f;
    KeyFrame3[11].movLum_z = 2.75f;
    KeyFrame3[11].giroLumY = 0.0f;
    KeyFrame3[11].giroLumX = 7.5f;
    KeyFrame3[11].giroLumZ = 0.0f;
    KeyFrame3[11].giroPIzqL = 0.0f;
    KeyFrame3[11].giroPDerL = 45.0f;
    KeyFrame3[11].giroBIzqL = 8.0f;
    KeyFrame3[11].giroBDerL = -79.0f;

    KeyFrame3[12].movLum_x = 0.0f;
    KeyFrame3[12].movLum_y = 0.0f;
    KeyFrame3[12].movLum_z = 3.0f;
    KeyFrame3[12].giroLumY = 0.0f;
    KeyFrame3[12].giroLumX = 0.0f;
    KeyFrame3[12].giroLumZ = 0.0f;
    KeyFrame3[12].giroPIzqL = 0.0f;
    KeyFrame3[12].giroPDerL = 0.0f;
    KeyFrame3[12].giroBIzqL = -12.0f;
    KeyFrame3[12].giroBDerL = -78.0f;

    KeyFrame3[13].movLum_x = 0.0f;
    KeyFrame3[13].movLum_y = 0.0f;
    KeyFrame3[13].movLum_z = 3.0f;
    KeyFrame3[13].giroLumY = -60.0f;
    KeyFrame3[13].giroLumX = 0.0f;
    KeyFrame3[13].giroLumZ = 0.0f;
    KeyFrame3[13].giroPIzqL = 0.0f;
    KeyFrame3[13].giroPDerL = 0.0f;
    KeyFrame3[13].giroBIzqL = 0.0f;
    KeyFrame3[13].giroBDerL = -78.0f;

    KeyFrame3[14].movLum_x = 0.0f;
    KeyFrame3[14].movLum_y = 0.0f;
    KeyFrame3[14].movLum_z = 3.0f;
    KeyFrame3[14].giroLumY = -120.0f;
    KeyFrame3[14].giroLumX = 0.0f;
    KeyFrame3[14].giroLumZ = 0.0f;
    KeyFrame3[14].giroPIzqL = 0.0f;
    KeyFrame3[14].giroPDerL = 0.0f;
    KeyFrame3[14].giroBIzqL = 0.0f;
    KeyFrame3[14].giroBDerL = -78.0f;

    KeyFrame3[15].movLum_x = 0.0f;
    KeyFrame3[15].movLum_y = 0.0f;
    KeyFrame3[15].movLum_z = 3.0f;
    KeyFrame3[15].giroLumY = -180.0f;
    KeyFrame3[15].giroLumX = 0.0f;
    KeyFrame3[15].giroLumZ = 0.0f;
    KeyFrame3[15].giroPIzqL = 0.0f;
    KeyFrame3[15].giroPDerL = 0.0f;
    KeyFrame3[15].giroBIzqL = 0.0f;
    KeyFrame3[15].giroBDerL = -78.0f;

    KeyFrame3[16].movLum_x = 0.0f;
    KeyFrame3[16].movLum_y = -0.2f;
    KeyFrame3[16].movLum_z = 3.0f;
    KeyFrame3[16].giroLumY = -180.0f;
    KeyFrame3[16].giroLumX = 0.0f;
    KeyFrame3[16].giroLumZ = -45.0f;
    KeyFrame3[16].giroPIzqL = 0.0f;
    KeyFrame3[16].giroPDerL = 0.0f;
    KeyFrame3[16].giroBIzqL = 0.0f;
    KeyFrame3[16].giroBDerL = -78.0f;

    KeyFrame3[17].movLum_x = 0.0f;
    KeyFrame3[17].movLum_y = -0.4f;
    KeyFrame3[17].movLum_z = 3.0f;
    KeyFrame3[17].giroLumY = -180.0f;
    KeyFrame3[17].giroLumX = 0.0f;
    KeyFrame3[17].giroLumZ = -90.0f;
    KeyFrame3[17].giroPIzqL = 0.0f;
    KeyFrame3[17].giroPDerL = 0.0f;
    KeyFrame3[17].giroBIzqL = 0.0f;
    KeyFrame3[17].giroBDerL = -78.0f;
    
    //Wanda
    KeyFrame4[0].movXW = 0.0f;
    KeyFrame4[0].movYW = 0.0f;
    KeyFrame4[0].movZW = 0.0f;
    KeyFrame4[0].giroYW = 0.0f;
    
    KeyFrame4[1].movXW = -0.25f;
    KeyFrame4[1].movYW = 0.05f;
    KeyFrame4[1].movZW = -0.067f;
    KeyFrame4[1].giroYW = 30.0f;
    
    KeyFrame4[2].movXW = -0.433f;
    KeyFrame4[2].movYW = 0.0f;
    KeyFrame4[2].movZW = -0.25f;
    KeyFrame4[2].giroYW = 60.0f;
    
    KeyFrame4[3].movXW = -0.5f;
    KeyFrame4[3].movYW = 0.05f;
    KeyFrame4[3].movZW = -0.5f;
    KeyFrame4[3].giroYW = 90.0f;
    
    KeyFrame4[4].movXW = -0.433f;
    KeyFrame4[4].movYW = 0.0f;
    KeyFrame4[4].movZW = -0.75f;
    KeyFrame4[4].giroYW = 120.0f;
    
    KeyFrame4[5].movXW = -0.25f;
    KeyFrame4[5].movYW = 0.05f;
    KeyFrame4[5].movZW = -0.933f;
    KeyFrame4[5].giroYW = 150.0f;
    
    KeyFrame4[6].movXW = 0.0f;
    KeyFrame4[6].movYW = 0.0f;
    KeyFrame4[6].movZW = -1.0f;
    KeyFrame4[6].giroYW = 180.0f;
    //7 8
    KeyFrame4[7].movXW = 0.25f;
    KeyFrame4[7].movYW = 0.05f;
    KeyFrame4[7].movZW = -0.933f;
    KeyFrame4[7].giroYW = 210.0f;
    
    KeyFrame4[8].movXW = 0.433f;
    KeyFrame4[8].movYW = 0.0f;
    KeyFrame4[8].movZW = -0.75f;
    KeyFrame4[8].giroYW = 240.0f;
    
    KeyFrame4[9].movXW = 0.5f;
    KeyFrame4[9].movYW = 0.05f;
    KeyFrame4[9].movZW = -0.5f;
    KeyFrame4[9].giroYW = 270.0f;
    
    KeyFrame4[10].movXW = 0.433f;
    KeyFrame4[10].movYW = 0.0f;
    KeyFrame4[10].movZW = -0.25f;
    KeyFrame4[10].giroYW = 300.0f;
    
    KeyFrame4[11].movXW = 0.25f;
    KeyFrame4[11].movYW = 0.05f;
    KeyFrame4[11].movZW = -0.067f;
    KeyFrame4[11].giroYW = 330.0f;
    
    KeyFrame4[12].movXW = 0.0f;
    KeyFrame4[12].movYW = 0.0f;
    KeyFrame4[12].movZW = 0.0f;
    KeyFrame4[12].giroYW = 360.0f;
    
}

void colocarCabina()
{
        glm::mat4 model(1.0);
        glm::mat4 model_aux(1.0);
    
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(7.25f, 2.0f, -20.55f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        cabina_T.UseTexture();
        meshList[25]->RenderMesh();

        //Puerta_Cabina
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.5f, -0.025f, 0.6f));
        model = glm::rotate(model, puerta * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 1.95f, 0.2f));
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        puerta_T.UseTexture();
        meshList[26]->RenderMesh();
        glDisable(GL_BLEND);
    
    //onomatopeya
    if(ropa == 1 and tiempo_transform > 0.0f and tiempo_transform < 2.0f ) {
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, -0.5f, 1.0f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        onomatopeya_T.UseTexture();
        meshList[24]->RenderMesh();
        glDisable(GL_BLEND);
    }

}

void animarAuto()
{
    switch (parte) {
        case 0:
            //camino recto
            if(movCocheX < 46.0f) movCocheX += movOffset * deltaTime;
            else if(angRot < 90.0f) {
                movOffset = 1.7f;
                movCocheX += movOffset * deltaTime;
                angRot += angOffset;
                movOffset = 0.9f;
                movCocheZ += movOffset * deltaTime;
            } else parte = 1;
            break;
        case 1:
            //camino recto
            if(movCocheZ < 44.0) {
                movOffset = 1.8f;
                movCocheZ += movOffset * deltaTime;
            } else if(angRot < 180.0f) {
                    movOffset = 0.9f;
                    movCocheX -= movOffset * deltaTime;
                    angRot += angOffset;
                    movOffset = 1.7f;
                    movCocheZ += movOffset * deltaTime;
            } else  parte = 2;
            break;
        case 2:
            //camino recto
            if(movCocheX > -46.0f) movCocheX -= movOffset * deltaTime;
            else if(angRot < 270.0f) {
                movOffset = 1.7f;
                movCocheX -= movOffset * deltaTime;
                angRot += angOffset;
                movOffset = 0.9f;
                movCocheZ -= movOffset * deltaTime;
            } else  parte = 3;
            break;
        case 3:
            //camino recto
            if(movCocheZ > -44.0) {
                movOffset = 1.8f;
                movCocheZ -= movOffset * deltaTime;
            } else if(angRot < 360.0f) {
                    movOffset = 0.9f;
                    movCocheX += movOffset * deltaTime;
                    angRot += angOffset;
                    movOffset = 1.7f;
                    movCocheZ -= movOffset * deltaTime;
            } else  if (movCocheX < -35.0f) movCocheX += movOffset * deltaTime;
            else {
                parte = 0;
                movCocheZ = -49.0f;
                movCocheX = -35.0f;
                angRot = 0;
            }
        default:
            break;
    }
}

void espectaculoLuces( int pointLightCount, int spotLightCount, bool encendido, GLfloat tiempo)
{
    if (tiempo >= 30 && tiempo <=90)
    {
        //faro noroeste 1
        pointLights[0] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            -8.25f, 2.5f, -17.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro noroeste 2
        pointLights[1] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            -8.25f, 2.5f, -37.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro noreste 1
        pointLights[2] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            8.25f, 2.5f, -27.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

    //faro suroeste 1
        pointLights[3] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            -8.25f, 2.5f, 27.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

         //faro sureste 1
        pointLights[4] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            8.25f, 2.5f, 17.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro sureste 2
        pointLights[5] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            8.25f, 2.5f, 37.75f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro oeste 1
        pointLights[6] = PointLight(0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, //intensidad ambiental
            -24.0f, 2.5f, 8.0f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro oeste 2
        pointLights[7] = PointLight(0.0f, 0.0f, 0.0f, //luz blanca
            0.0f, 0.0f, //intensidad ambiental
            -24.0f, 2.5f, -8.0f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro este 1
        pointLights[8] = PointLight(0.0f, 0.0f, 0.0f, //luz blanca
            0.0f, 0.0f, //intensidad ambiental
            24.0f, 2.5f, 8.0f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes

        //faro este 2
        pointLights[9] = PointLight(0.0f, 0.0f, 0.0f, //luz blanca
            0.0f, 0.0f, //intensidad ambiental
            24.0f, 2.5f, -8.0f, //posicion (misma que un castillo circular del kiosko)
            1.0f, 0.0f, 0.0f);  //coeficientes
        //spotLightCount = 1;
        
        if (tiempo < 45 && tiempo >= 30)
        {
            //luz amarilla
            spotLights[0] = SpotLight(0.5882f, 0.0f, 0.8784f, //luz amarilla
                0.0f, 5.0f, //intensidad ambiental
                2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                pos_actL1, -1.0f, -1.15f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                10.0f); // es el angulo del cono

            //luz verde
            spotLights[1] = SpotLight(0.5882f, 0.0f, 0.8784f, //luz verde
                0.0f, 5.0f, //intensidad ambiental
                2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                pos_actL1, -1.0f, 1.15f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                10.0f); // es el angulo del cono



            //luz roja

            spotLights[2] = SpotLight(0.6157f, 0.8863f, 0.0667f, //luz roja
                0.0f, 5.0f, //intensidad ambiental
                -2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                pos_actL2, -1.0f, 1.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                10.0f); // es el angulo del cono

            //luz roja

            spotLights[3] = SpotLight(0.6157f, 0.8863f, 0.0667f, //luz roja
                0.0f, 5.0f, //intensidad ambiental
                -2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                pos_actL2, -1.0f, -1.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                10.0f); // es el angulo del cono

              //luz roja

            spotLights[4] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                0.0f, 8.0f, //intensidad ambiental
                4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                1.0f, 0.0f, pos_actLC, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                30.0f); // es el angulo del cono

            spotLights[5] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                0.0f, 8.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, -pos_actLC, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                30.0f); // es el angulo del cono


            if (pos_actL2 >= 1.3 || pos_actL2 <= -1)
            {
                pos_sigL2 = -1 * pos_sigL2;
            }

            if (pos_actLC >= 1.5 || pos_actLC <= -1.5) //Cambiar sentido
            {
                pos_sigLC = -1 * pos_sigLC;
            }
            if (pos_actL1 >= 1 || pos_actL1 <= -1.3)
            {
                pos_sigL1 = -1 * pos_sigL1;
            }

            pos_actLC = pos_actLC + pos_sigLC;

            pos_actL1 = pos_actL1 + pos_sigL1;

            pos_actL2 = pos_actL2 + pos_sigL2;

        }

        if (tiempo < 60 && tiempo >= 45)
        {
            spotLights[2] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            spotLights[3] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            spotLights[4] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            spotLights[5] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            if (((int)tiempo) % 2 == 0)
            {
                

                spotLights[0] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                    0.0f, 8.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    30.0f); // es el angulo del cono

                spotLights[1] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                    0.0f, 8.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    1.0f, 0.0f, 0.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    30.0f); // es el angulo del cono
            }

            else
            {

                spotLights[0] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                    0.0f, 8.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, 0.0f, 1.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    50.0f); // es el angulo del cono

                spotLights[1] = SpotLight(0.8902f, 0.4509f, 0.1058f, //luz roja
                    0.0f, 8.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, 0.0f, -1.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    50.0f); // es el angulo del cono

            }


        }
        if (tiempo < 75 && tiempo >= 60)
        {
            spotLights[4] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            spotLights[5] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            if (((int)tiempo) % 2 == 0)
            {
                //luz azul
                spotLights[0] = SpotLight(0.0274f, 0.7647f, 0.8039f,
                    0.0f, 8.0f,
                    0.0f, 9.0f, 0.0f, //posiciÛn
                    0.0f, -1.0f, 0.0, //direcciÛn
                    1.0f, 0.0f, 0.0f,
                    80.0f); // es el angulo del cono

                spotLights[1] = SpotLight(0.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    0.0f); // es el angulo del cono

                spotLights[2] = SpotLight(0.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    0.0f); // es el angulo del cono

                spotLights[3] = SpotLight(0.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, //intensidad ambiental
                    -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                    -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    0.0f); // es el angulo del cono


            }

            else
            {
                spotLightCount = 4;
                rojo1 = rand() % 1000;
                verde1 = rand() % 1000;
                azul1 = rand() % 1000;

                rojo2 = rand() % 1000;
                verde2 = rand() % 1000;
                azul2 = rand() % 1000;

                //luz amarilla
                spotLights[0] = SpotLight(rojo2 / 1000, verde2 / 1000, azul2 / 1000, //luz amarilla
                    0.0f, 5.0f, //intensidad ambiental
                    2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, -1.0f, -1.15f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    40.0f); // es el angulo del cono

                //luz verde
                spotLights[1] = SpotLight(rojo1 / 1000, verde1 / 1000, azul1 / 1000, //luz verde
                    0.0f, 5.0f, //intensidad ambiental
                    2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, -1.0f, 1.15f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    40.0f); // es el angulo del cono

                //luz roja

                spotLights[2] = SpotLight(rojo2 / 1000, verde2 / 1000, azul2 / 1000, //luz roja
                    0.0f, 5.0f, //intensidad ambiental
                    -2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, -1.0f, 1.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    40.0f); // es el angulo del cono

                //luz roja

                spotLights[3] = SpotLight(rojo1 / 1000, verde1 / 1000, azul1 / 1000, //luz roja
                    0.0f, 5.0f, //intensidad ambiental
                    -2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                    0.0f, -1.0f, -1.0f, //vector de direcciÛn
                    1.0f, 0.0f, 0.0f,  //coeficientes
                    40.0f); // es el angulo del cono

            }


        }


        if (tiempo < 90 && tiempo >= 75)
        {


            spotLights[4] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            spotLights[5] = SpotLight(0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, //intensidad ambiental
                -4.5f, 8.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                -1.0f, 0.0f, 0.0f, //vector de direcciÛn
                1.0f, 0.0f, 0.0f,  //coeficientes
                0.0f); // es el angulo del cono

            tonoColor = pos_actLC;

            if (pos_actLC <= 0)
            {
                tonoColor = (-1) * (pos_actLC);
            }

            //luz azul
            spotLights[0] = SpotLight(0.5411f, 0.7882f, tonoColor / 1.5,
                0.0f, 8.0f,
                0.0f, 9.0f, 0.0f, //posiciÛn
                0.0f, -1.0f, pos_actLC, //direcciÛn
                1.0f, 0.0f, 0.0f,
                30.0f); // es el angulo del cono

            //luz azul
            spotLights[1] = SpotLight(0.9804f, 0.4117f, tonoColor / 1.5,
                0.0f, 8.0f,
                0.0f, 9.0f, 0.0f, //posiciÛn
                -pos_actLC, -1.0f, 0.0f, //direcciÛn
                1.0f, 0.0f, 0.0f,
                30.0f); // es el angulo del cono

            //luz azul
            spotLights[2] = SpotLight(0.0392f, 0.6980f, tonoColor / 1.5,
                0.0f, 8.0f,
                0.0f, 9.0f, 0.0f, //posiciÛn
                pos_actLC, -1.0f, 0.0f, //direcciÛn
                1.0f, 0.0f, 0.0f,
                30.0f); // es el angulo del cono

            //luz azul
            spotLights[3] = SpotLight(0.8980f, 0.8000f, tonoColor / 1.5,
                0.0f, 8.0f,
                0.0f, 9.0f, 0.0f, //posiciÛn
                0.0f, -1.0f, -pos_actLC, //direcciÛn
                1.0f, 0.0f, 0.0f,
                30.0f); // es el angulo del cono

            if (pos_actLC >= 1.5 || pos_actLC <= -1.5) //Cambiar sentido
            {
                pos_sigLC = -1 * pos_sigLC;
            }

            pos_actLC = pos_actLC + pos_sigLC;

        }


        //tiempo += relojtiempo;
    }
    // Espectaculo de Luces (Fin)

    else
    {
       if (!encendido)
                {
                    //faro noroeste 1
                    pointLights[0] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, -17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro noroeste 2
                    pointLights[1] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, -36.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro noreste 1
                    pointLights[2] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        9.0f, 2.5f, -27.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                     //Kiosko
                    //Luz central 1
                    spotLights[0] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        0.0f, 9.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 0.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono
                    //Luz central 2
                    spotLights[1] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        0.0f, 9.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 0.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono

                    //luz 1
                    spotLights[2] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, -1.15f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono

                    //luz 2
                    spotLights[3] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 1.15f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono

                    //luz 3
                    spotLights[4] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        -2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 1.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono

                    //luz 4
                    spotLights[5] = SpotLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.15f, 0.3f, //intensidad ambiental
                        -2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, -1.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        50.0f); // es el angulo del cono

                //faro suroeste 1
                    pointLights[3] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        9.0f, 2.5f, 17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                     //faro sureste 1
                    pointLights[4] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, 17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro sureste 2
                    pointLights[5] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                       -7.5f, 2.5f, 36.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro oeste 1
                    pointLights[6] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -24.0f, 2.5f, 8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro oeste 2
                    pointLights[7] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -24.0f, 2.5f, -8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro este 1
                    pointLights[8] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        24.0f, 2.5f, 8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro este 2
                    pointLights[9] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        24.0f, 2.5f, -8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes
                    //spotLightCount = 1;
                }
                else
                {
                
                    //faro noroeste 1
                    pointLights[0] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, -17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro noroeste 2
                    pointLights[1] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, -36.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro noreste 1
                    pointLights[2] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        9.0f, 2.5f, -27.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                //Kiosko
                    //Luz central 1
                    spotLights[0] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        0.0f, 9.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 0.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //Luz central 2
                    spotLights[1] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        0.0f, 9.0f, 0.0f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 0.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //luz 1
                    spotLights[2] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, -1.15f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //luz 2
                    spotLights[3] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 1.15f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //luz 3

                    spotLights[4] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        -2.5f, 8.0f, -4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, 1.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //luz 4

                    spotLights[5] = SpotLight(0.0f, 0.0f, 0.0f, //luz blanca
                        0.0f, 0.0f, //intensidad ambiental
                        -2.5f, 8.0f, 4.5f, //posicion (misma que un castillo circular del kiosko)
                        0.0f, -1.0f, -1.0f, //vector de direcciÛn
                        1.0f, 0.0f, 0.0f,  //coeficientes
                        0.0f); // es el angulo del cono

                    //faro suroeste 1
                    pointLights[3] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        9.0f, 2.5f, 17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                     //faro sureste 1
                    pointLights[4] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, 17.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro sureste 2
                    pointLights[5] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -7.5f, 2.5f, 36.75f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro oeste 1
                    pointLights[6] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -24.0f, 2.5f, 8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro oeste 2
                    pointLights[7] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        -24.0f, 2.5f, -8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro este 1
                    pointLights[8] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        24.0f, 2.5f, 8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes

                    //faro este 2
                    pointLights[9] = PointLight(1.0f, 1.0f, 1.0f, //luz blanca
                        0.0f, 0.2f, //intensidad ambiental
                        24.0f, 2.5f, -8.25f, //posicion (misma que un castillo circular del kiosko)
                        1.0f, 0.0f, 0.0f);  //coeficientes
                    //spotLightCount = 1;
                }
            }
        }

void colocarAuto()
{
    glm::mat4 model(1.0);
    glm::mat4 model_aux(1.0);
    
    //Auto
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(movCocheX, 0.9f, movCocheZ));
    model = glm::rotate(model, angRot * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    //model = glm::rotate(model, angRotY * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
    model_aux = model;
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    auto_M.RenderModel();
    
    //llanta 1
    model = model_aux;
    model = glm::translate(model, glm::vec3(2.4f, 0.1f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, movLlanta * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    llanta_M.RenderModel();
    
    //llanta 2
    model = model_aux;
    model = glm::translate(model, glm::vec3(-2.4f, 0.1f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, movLlanta * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    llanta_M.RenderModel();
    
    //llanta 3
    model = model_aux;
    model = glm::translate(model, glm::vec3(2.4f, 0.1f, -2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, movLlanta * toRadians, glm::vec3(-1.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    llanta_M.RenderModel();
    
    //llanta 4
    model = model_aux;
    model = glm::translate(model, glm::vec3(-2.4f, 0.1f, -2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, movLlanta * toRadians, glm::vec3(-1.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    llanta_M.RenderModel();
    
}

void colocarRejas() {
    glm::mat4 model(1.0);
    
    //noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-28.5f, 0.2f, -37.75f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.9f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    reja_M.RenderModel();
    
    //noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(39.8f, 0.2f, -27.7f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.9f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    reja_M.RenderModel();
    
    //suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-39.8f, 0.2f, 27.75f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.9f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    reja_M.RenderModel();
    
    //sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(28.5f, 0.2f, 37.75f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.9f));
    model = glm::rotate(model, 180 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    reja_M.RenderModel();
    
    
}
void colocarObjetos() {
    glm::mat4 model(1.0);
    
    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 29.95f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    banoMTexture.UseTexture();
    meshList[23]->RenderMesh();

    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 28.85f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 27.75f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();
    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 26.65f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 25.55f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Mujer
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(7.25f, 2.0f, 24.45f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, -90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -29.95f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    banoHTexture.UseTexture();
    meshList[23]->RenderMesh();

    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -28.85f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -27.75f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();
    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -26.65f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -25.55f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();

    //Baño Hombre
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.25f, 2.0f, -24.45f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[23]->RenderMesh();
    
    // faro noroeste 1
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.5f, 0.0f, -17.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    lantern_M.RenderModel();


    //faro noroeste 2
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.5f, 0.0f, -36.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faro noreste 3

    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(9.0f, 0.0f, -27.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    // faro suroeste 1
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.5f, 0.0f, 17.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();


    //faro suroeste 2
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-7.5f, 0.0f, 36.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faro sureste 3

    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(9.0f, 0.0f, 17.75f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faros oeste 1
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 0.0f, 8.25f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faros oeste 2
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 0.0f, -8.25f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faros este 1
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 0.0f, 8.25f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();

    //faros este 2
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 0.0f, -8.25f));
    model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    lantern_M.RenderModel();
    glDisable(GL_BLEND);
    
    //Botes
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-8.5f, 0.76f, -12.0f));
    model = glm::scale(model, glm::vec3(1.0f, 1.5f, 1.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    bote_T.UseTexture();
    meshList[22]->RenderMesh();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(18.0f, 0.76f, -8.5f));
    model = glm::scale(model, glm::vec3(1.0f, 1.5f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[22]->RenderMesh();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-8.5f, 0.76f, 12.0f));
    model = glm::scale(model, glm::vec3(1.0f, 1.5f, 1.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[22]->RenderMesh();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-38.0f, 0.76f, -8.5f));
    model = glm::scale(model, glm::vec3(1.0f, 1.5f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[22]->RenderMesh();
    
    //fuente
     model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 1.5f, 38.0f));
     //model = glm::scale(model, glm::vec3(6.0f, 9.0f, 6.0f));
     glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    fuente_M.RenderModel();
     meshList[21]->RenderMesh();

    
    //fuente
     model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 1.5f, -38.0f));
     //model = glm::scale(model, glm::vec3(6.0f, 9.0f, 6.0f));
     glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
     //blending: transparencia o traslucidez
    fuente_M.RenderModel();
     meshList[21]->RenderMesh();

}

void colocarArboles() {
    glm::mat4 model(1.0);
    
    //noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-13.0f, 4.0f, -13.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-22.0f, 4.0f, -20.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-26.0f, 2.3f, -22.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-34.0f, 4.0f, -28.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 4.0f, -34.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-14.0f, 4.0f, -30.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-12.0f, 2.3f, -24.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    //noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(13.0f, 4.0f, -13.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(22.0f, 4.0f, -20.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(26.0f, 2.3f, -22.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(34.0f, 4.0f, -28.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 4.0f, -34.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(14.0f, 4.0f, -30.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(12.0f, 2.3f, -24.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    //suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-13.0f, 4.0f, 13.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-22.0f, 4.0f, 20.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-26.0f, 2.3f, 22.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-34.0f, 4.0f, 28.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 4.0f, 34.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-14.0f, 4.0f, 30.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-12.0f, 2.3f, 24.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    //sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(13.0f, 4.0f, 13.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(22.0f, 4.0f, 20.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(26.0f, 2.3f, 22.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(34.0f, 4.0f, 28.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 4.0f, 34.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(14.0f, 4.0f, 30.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(12.0f, 2.3f, 24.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbol_M.RenderModel();
    
}
void colocarArbustos() {
    glm::mat4 model(1.0);
    
    //noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-9.0f, 1.0f, -9.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-10.0f, 0.5f, -10.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-22.0f, 1.0f, -9.5f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-32.0f, 1.0f, -12.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-37.0f, 0.5f, -17.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-35.0f, 0.5f, -25.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-36.0f, 0.5f, -31.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-18.0f, 1.0f, -19.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-11.0f, 1.0f, -29.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-20.0f, 0.5f, -19.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    //noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(9.0f, 1.0f, -9.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(10.0f, 0.5f, -10.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(22.0f, 1.0f, -9.5f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(32.0f, 1.0f, -12.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(37.0f, 0.5f, -17.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(35.0f, 0.5f, -25.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(36.0f, 0.5f, -31.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(18.0f, 1.0f, -19.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(11.0f, 1.0f, -29.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(20.0f, 0.5f, -19.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();

    //suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-9.0f, 1.0f, 9.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-10.0f, 0.5f, 10.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-22.0f, 1.0f, 9.5f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-32.0f, 1.0f, 12.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-37.0f, 0.5f, 17.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-35.0f, 0.5f, 25.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-36.0f, 0.5f, 31.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-18.0f, 1.0f, 19.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-11.0f, 1.0f, 29.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-20.0f, 0.5f, 19.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    //sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(9.0f, 1.0f, 9.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(10.0f, 0.5f, 10.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(22.0f, 1.0f, 9.5f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(32.0f, 1.0f, 12.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(37.0f, 0.5f, 17.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(35.0f, 0.5f, 25.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(36.0f, 0.5f, 31.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(18.0f, 1.0f, 19.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(11.0f, 1.0f, 29.0f));
    model = glm::scale(model, glm::vec3(0.09f, 0.09f, 0.09f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(20.0f, 0.5f, 19.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    arbusto_M.RenderModel();
    
}
void unirPiso()
{
    glm::mat4 model(1.0);
    
    //ACERAS
    
    //acera norte-kiosco
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -23.0f)); //desplaza 15 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    camino_T.UseTexture();
    meshList[0]->RenderMesh();
    
    //acera sur-kisco
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 23.0f)); //desplaza 15 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //acera oeste-kiosco
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.067f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //acera este-kiosco
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, -1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.067f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //ACERAS
    
    //acera oeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-44.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(0.5f, 1.0f, 2.5334f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    acera_T.UseTexture();
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //acera oeste 2
    model = glm::translate(model, glm::vec3(-40.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //acera este
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(44.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(0.5f, 1.0f, 2.5334f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //acera este 2
    model = glm::translate(model, glm::vec3(40.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //acera norte
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -42.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.5f, 1.0f, 2.67f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //acera norte 2
    model = glm::translate(model, glm::vec3(40.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //acera  sur
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 42.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, -1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.5f, 1.0f, 2.67f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //acera sur 2
    model = glm::translate(model, glm::vec3(40.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[0]->RenderMesh();
    
    //PASTOS
    
    //pasto noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 0.0f, -23.0f)); //desplaza 15 + 8 del kiosco
    model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    pasto_T.UseTexture();
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //pasto noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 0.0f, -23.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //pasto suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-24.0f, 0.0f, 23.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //psato sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(24.0f, 0.0f, 23.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    meshList[0]->RenderMesh();
    
    //CALLES
    
    //calle oeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-54.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    calle_T.UseTexture();
    meshList[17]->RenderMesh();
    
    //calle este
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(54.0f, 0.0f, 0.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[17]->RenderMesh();
    
    //calle norte
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -52.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(1.053f, 1.0f, 1.0f));
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[17]->RenderMesh();
    
    //calle sur
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 52.0f)); //desplaza 24 + 8 del kiosco
    model = glm::scale(model, glm::vec3(1.053f, 1.0f, 1.0f));
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, -1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[17]->RenderMesh();
    
    //esquinas
    
    //acera esquina noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-60.0f, 0.0f, -58.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    camino_T.UseTexture();
    meshList[18]->RenderMesh();
    
    //acera esquina noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(60.0f, 0.0f, -58.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, -1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[18]->RenderMesh();
    
    //acera esquina suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-60.0f, 0.0f, 58.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[18]->RenderMesh();
    
    //acera esquina sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(60.0f, 0.0f, 58.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 180 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[18]->RenderMesh();
    
    //calle esquina noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-48.0f, 0.0f, -50.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    esquina_calle_T.UseTexture();
    meshList[19]->RenderMesh();
    
    //calle esquina noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(52.0f, 0.0f, -46.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, -1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[19]->RenderMesh();
    
    //calle esquina seroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-52.0f, 0.0f, 46.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 90 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[19]->RenderMesh();
    
    //calle esquina sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(48.0f, 0.0f, 50.0f)); //desplaza 24 + 8 del kiosco
    model = glm::rotate(model, 180 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[19]->RenderMesh();
    
    //rectangulo noroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-44.0f, 0.0f, -42.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    camino_T.UseTexture();
    meshList[20]->RenderMesh();
    
    //rectangulo noreste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(44.0f, 0.0f, -42.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[20]->RenderMesh();
    
    //rectangulo suroeste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(-44.0f, 0.0f, 42.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[20]->RenderMesh();
    
    //rectangulo sureste
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(44.0f, 0.0f, 42.0f)); //desplaza 24 + 8 del kiosco
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[20]->RenderMesh();
    
    //CENTRO KIOSCO
    model = glm::mat4(1.0);
    model = glm::scale(model, glm::vec3(2.0f, 1.0f, 2.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    loceta_T.UseTexture();
    meshList[20]->RenderMesh();
}

void UnirSuperman (float posX, float posY, float posZ)
{
    glm::mat4 model(1.0);
    glm::mat4 model_aux(1.0);
    glm::mat4 model_brazo_cabeza(1.0);
    
    //instancia torzo superman
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(posX + movSup_x, posY, posZ + movSup_z));
    model = glm::rotate(model, (90 + giroSup) * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, giroSy * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model_aux = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    if(!ropa) lex_T.UseTexture(); else if (ropa == 1) flash_T.UseTexture();  else T_super.UseTexture();
    meshList[3]->RenderMesh();
    
    //capa
    if(ropa > 1) {
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, -0.13f, -0.1f));
        model = glm::scale(model, glm::vec3(0.65f, 0.6f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        T_rojo.UseTexture();
        meshList[9]->RenderMesh();
    }
    
    //instancia cabeza superman
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.39f, 0.23f, 0.39f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    if(ropa == 1) facesorp_T.UseTexture(); else faceTexture.UseTexture();
    meshList[2]->RenderMesh();
    
    //instancia cabello
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
    model = glm::scale(model, glm::vec3(0.69f, 0.4f, 0.69f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    if(!ropa) lex_T.UseTexture(); else if (ropa == 1) flash_T.UseTexture();  else T_super.UseTexture();
    meshList[1]->RenderMesh();
    
    //instancia cadera superman
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, -0.2595f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.21f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[7]->RenderMesh();
    
    //instanciar pierna izquierda
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    model = glm::rotate(model, giroPIzq * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran las piernas
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();
    
    //instanciar pierna derecha
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(-0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    model = glm::rotate(model, giroPDer * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran las piernas
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();
    
    //instanciar brazo izquierdo superman
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.3f, -0.05f, 0.2f));
    model = glm::rotate(model, giroBIzq * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran los brazos
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[4]->RenderMesh(); //4
    
    //instancia mano izquierda
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model = glm::scale(model, glm::vec3(0.203f, 0.15f, 0.1452f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    pielTexture.UseTexture();
    meshList[6]->RenderMesh();
    
    //instanciar brazo derecho superman
    model = model_aux;
    model = glm::translate(model, glm::vec3(-0.3f, -0.05f, 0.2f));
    model = glm::rotate(model, giroBDer * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran los brazos
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    if(!ropa) lex_T.UseTexture(); else if (ropa == 1) flash_T.UseTexture();  else T_super.UseTexture();
    meshList[5]->RenderMesh(); //4
    
    //instancia mano izquierda
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model = glm::scale(model, glm::vec3(0.203f, 0.15f, 0.1452f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    pielTexture.UseTexture();
    meshList[6]->RenderMesh();
}

void UnirLumpy()
{
    glm::mat4 model(1.0);
    glm::mat4 model_aux(1.0);
    glm::mat4 model_aux2(1.0);
    glm::mat4 model_brazo_cabeza(1.0);
    glm::mat4 model_mano(1.0);
    
    //instancia torzo Lumpy
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(posXL + movLum_x, posYL + movLum_y, posZL + movLum_z));
    model = glm::rotate(model, giroLumY * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, giroLumZ * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_aux = model;
    model = glm::rotate(model, giroLumX * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model_aux2 = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_slumpy.UseTexture();
    meshList[3]->RenderMesh();
    
    //instancia cabeza Lumpy
    model = model_aux2;
    model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.39f, 0.23f, 0.39f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    if (activaLF)
    {
        caraLumpyM.UseTexture();
    }
    else
    {
        caraLumpy.UseTexture();
    }
    meshList[2]->RenderMesh();
    
    //instancia cuernitos 1
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.53f, 0.18f, 0.0f));
    model = glm::rotate(model, 180 * toRadians ,glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.35f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_cuernosF.UseTexture();
    meshList[10]->RenderMesh();
    
    //instancia cuernitos 2
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(-0.53f, 0.18f, 0.0f));
    model = glm::rotate(model, 180 * toRadians ,glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.35f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[10]->RenderMesh();
    
    //instancia cadera Lumpy
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, -0.2595f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.21f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_slumpy.UseTexture();
    meshList[7]->RenderMesh();
    
    //instanciar pierna izquierda Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    model = glm::rotate(model, giroPIzqL * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran las piernas
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();
    
    //instanciar pierna derecha Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(-0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    model = glm::rotate(model, giroPDerL * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran las piernas
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();
    
    //instanciar brazo izquierdo Lumpy
    model = model_aux2;
    model = glm::translate(model, glm::vec3(0.3f, -0.05f, 0.2f));
    model = glm::rotate(model, giroBIzqL * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran los brazos
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[4]->RenderMesh(); //4
    
    //instancia mano izquierda Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model = glm::scale(model, glm::vec3(0.203f, 0.075f, 0.1452f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[6]->RenderMesh();
    
    //instanciar brazo derecho Lumpy
    model = model_aux2;
    //model = glm::translate(model, glm::vec3(-0.3f, -0.05f, 0.2f));
    model = glm::translate(model, glm::vec3(-0.3f, 0.38f, 0.2f));
    model = glm::rotate(model, giroBDerL * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran los brazos
    //model = glm::rotate(model, giroBDerL * toRadians, glm::vec3(1.0f, 0.0f, 0.0f)); //Ver como giran los brazos
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[5]->RenderMesh(); //4
    
    //instancia mano derecha Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model_mano = model;
    model = glm::scale(model, glm::vec3(0.103f, 0.15f, 0.1452f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[6]->RenderMesh();

    //Boomerang

    if (boomerangMano)
    {
        model = model_mano;
        //model = glm::translate(model, glm::vec3(posxB, 3.0f, poszB));
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
        model = glm::rotate(model, giroB, glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        faro_T.UseTexture();
        meshList[27]->RenderMesh(); //poner el √≠ndice correcto
    }
    else
    {
        if (boomerangFijo)
        {
            model = model_brazo_cabeza;
            //model = glm::translate(model, glm::vec3(posxB, 3.0f, poszB));
            model = glm::translate(model, glm::vec3(0.5f, 0.0f, 0.0f));
            model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
            model = glm::rotate(model, giroB, glm::vec3(0.0f, 0.0f, 1.0f));
            glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
            faro_T.UseTexture();
            meshList[27]->RenderMesh(); //poner el √≠ndice correcto
        
        }

        else
        {
            model = glm::mat4(1.0);
            //model = glm::translate(model, glm::vec3(posxB, 3.0f, poszB));
            model = glm::translate(model, glm::vec3(posxB, 1.0f, poszB));
            model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
            model = glm::rotate(model, giroB, glm::vec3(0.0f, 0.0f, 1.0f));
            glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
            faro_T.UseTexture();
            meshList[27]->RenderMesh(); //poner el √≠ndice correcto
        }
    }
    
}

void UnirLumpyFantasma()
{
    glm::mat4 model(1.0);
    glm::mat4 model_aux(1.0);
    glm::mat4 model_brazo_cabeza(1.0);

    //instancia torzo Lumpy
    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(20.0f, 0.55f + subida, 0.0f));
    model = glm::rotate(model, (180 + giroLF) * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model_aux = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_slumpyF.UseTexture();
    meshList[3]->RenderMesh();

    //instancia cabeza Lumpy
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.39f, 0.23f, 0.39f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    caraLumpyF.UseTexture();
    meshList[2]->RenderMesh();

    //Halo
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.35f, 0.0f));
    model = glm::scale(model, glm::vec3(0.005f, 0.005f, 0.005f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    halo_M.RenderModel();

    //instancia cuernitos 1
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.53f, 0.18f, 0.0f));
    model = glm::rotate(model, 180 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.35f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_cuernos.UseTexture();
    meshList[10]->RenderMesh();

    //instancia cuernitos 2
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(-0.53f, 0.18f, 0.0f));
    model = glm::rotate(model, 180 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.35f, 0.4f, 0.4f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[10]->RenderMesh();

    //instancia cadera Lumpy
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, -0.2595f, 0.0f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.59f, 0.21f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    T_slumpyF.UseTexture();
    meshList[7]->RenderMesh();

    //instanciar pierna Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();

    //instanciar pierna Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(-0.17f, -0.1f, 0.0f));
    model = glm::scale(model, glm::vec3(0.254f, 0.33f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[8]->RenderMesh();

    //instanciar brazo izquierdo Lumpy
    model = model_aux;
    model = glm::translate(model, glm::vec3(0.3f, -0.05f, 0.2f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[4]->RenderMesh(); //4

    //instancia mano izquierda Lumpy
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model = glm::scale(model, glm::vec3(0.203f, 0.075f, 0.1452f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[6]->RenderMesh();

    //instanciar brazo derecho Lumpy
    model = model_aux;
    model = glm::translate(model, glm::vec3(-0.3f, -0.05f, 0.2f));
    model_brazo_cabeza = model;
    model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.3f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[5]->RenderMesh(); //4

    //instancia mano izquierda
    model = model_brazo_cabeza;
    model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.075f));
    model = glm::scale(model, glm::vec3(0.203f, 0.075f, 0.1452f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    meshList[6]->RenderMesh();
     
    //Alas

    model = model_aux;
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.15f));
    model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.5f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    T_cuernosF.UseTexture();
    meshList[28]->RenderMesh();
}


void UnirKiosco()
{
    GLuint uniformSpecularIntensity = 0, uniformShininess = 0;;
    glm::mat4 model(1.0);
    glm::mat4 model_aux(1.0);
    glm::mat4 model_aux2(1.0);
    glm::mat4 model_banca(1.0);
    glm::mat4 model_Auxbanca(1.0);
    glm::mat4 model_Decaedro(1.0);
    uniformSpecularIntensity = shaderList[0].GetSpecularIntensityLocation();
    uniformShininess = shaderList[0].GetShininessLocation();

    //Decaedro
    model_Decaedro = glm::mat4(1.0);
    model_Decaedro = glm::translate(model_Decaedro, glm::vec3(0.0, 6.8f, 0.0f));
    model_Decaedro = glm::scale(model_Decaedro, glm::vec3(0.025f, 0.025f, 0.025f));
    model_Decaedro = glm::rotate(model_Decaedro, 180 * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_Decaedro));
    Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
    decaedro_M.RenderModel();
    
    //Banca 1

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, -25.55f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 2

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, -23.55f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 3

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, -15.75f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    //Banca 4

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, -13.75f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 5

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-7.25f, 0.0f, 25.55f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 6

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-7.25f, 0.0f, 23.55f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 7

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, 15.75f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    //Banca 8

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(7.25f, 0.0f, 13.75f));
    model_banca = glm::rotate(model_banca, 90 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 9

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-22.0f, 0.0f, 7.0f));
    model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 10

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-20.0f, 0.0f, 7.0f));
    model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    //Banca 11

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-22.0f, 0.0f, -7.0f));
    // model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 12

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(-20.0f, 0.0f, -7.0f));
    //model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 13

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(22.0f, 0.0f, 7.0f));
    model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 14

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(20.0f, 0.0f, 7.0f));
    model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    //Banca 15

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(22.0f, 0.0f, -7.0f));
    // model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    //Banca 16

    model_banca = glm::mat4(1.0);
    model_banca = glm::translate(model_banca, glm::vec3(20.0f, 0.0f, -7.0f));
    //model_banca = glm::rotate(model_banca, 180 * toRadians, glm::vec3(0.0f, -1.0f, 0.0f));
    model_Auxbanca = model_banca;
    model_banca = glm::scale(model_banca, glm::vec3(1.0f, 1.0f, 1.0f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[14]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.125f, -0.065f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.63f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
   // model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.425f, -0.53f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.1f, 0.75f));
    // model_Auxbanca = model_banca;
     //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    roundBrickTexture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.225f, 0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();


    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.125f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.375f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(-0.625f, 0.2375f, -0.1275f));
    model_banca = glm::scale(model_banca, glm::vec3(0.25f, 0.125f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[16]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.4f, -0.4f));
    model_banca = glm::rotate(model_banca, 60 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();

    model_banca = model_Auxbanca;
    model_banca = glm::translate(model_banca, glm::vec3(0.0f, 0.0f, -1.025f));
    //model_banca = glm::scale(model_banca, glm::vec3(1.5f, 0.1f, 0.25f));
    //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    //model_Auxbanca = model_banca;
    //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model_banca));
    brick241Texture.UseTexture();
    meshList[15]->RenderMesh();
    
     //4to Rengln Izquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 1.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
         //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

       //Blastrado 3

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.6f, 0.0f, 0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

       //5to Escaln Izquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 2.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //4to Escaln Izquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 3.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3er Escaln Izquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 4.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2do EscalnIzquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 5.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1er Escaln Izquierdo Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 6.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3er Rengln Izquierdo Abajo
      //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -1.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.6f, 0.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //5to Escaln Izquierdo Abajo
      //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -2.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //4to Escaln Izquierdo Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -3.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3er Escaln izquierdo Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -4.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2do Escaln izquierdo Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -5.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1er Escaln Izquierdo Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -6.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2do Rengln Izquierdo  Origen Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, 0.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
         //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //1er Rengln Izquierda  Origen Bajo
      //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-6.0f, 0.25f, -0.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
         //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        

        //5ta FILA izquierda Origen
               //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-4.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();




        //5ta FILA Izquierda Arriba
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-4.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 3
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 3
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2
        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

    //5ta Fila Izquierda  Abajo
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-4.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Castillo 4
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 4
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(-0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2


        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();


        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //4ta FILA Izquierda Origen Arriba
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-3.5f, 0.25f, 1.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //4ta FILA izquierda Origen Abajo
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-3.5f, 0.25f, -1.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //4ta Fila Izquierda Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-3.5f, 0.25f, 3.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.6f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //4ta Fila Izquierda  Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-3.5f, 0.25f, -3.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

       //Blastrado 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-0.6f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //3ra FILA Izquierda Origen
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-2.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //3ra Fila Izquierda Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-2.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //3ra Fila Izquierda Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-2.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 1
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 1
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //3ra Fila Izquierda  Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-2.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
    
//3ra Fila Izquierda Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-2.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 2
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 2
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(-0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //2da FILA Izquierda Origen
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-1.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Izquierda Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-1.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Izquierda Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-1.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //2 Fila Izquierda  Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-1.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Izquierda Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-1.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1ra Fila Izquierda Origen
//Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-0.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila izquierda Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-0.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila Izquierda Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-0.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1ra Fila Izquierda Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(0.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila Izquierda Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-0.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();



        //1ra Fila Derecha Origen
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(0.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila Derecha Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(0.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila Derecha Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(0.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1ra Fila Derecha Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(-0.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1ra Fila Derecha Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(0.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2da FILA DERECHA Origen
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(1.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Derecha Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(1.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Derecha Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(1.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //2 Fila Derecha  Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(1.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //2da Fila Derecha Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(1.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3ra FILA DERECHA Origen
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(2.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //3ra Fila Derecha Arriba Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(2.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //3ra Fila Derecha Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(2.5f, 0.25f, 4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 1
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 1
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //3ra Fila Derecha  Abajo Medio
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(2.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //3ra Fila Derecha Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(2.5f, 0.25f, -4.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 2
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 2
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(-0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //4ta FILA DERECHA Origen Arriba
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(3.5f, 0.25f, 1.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //4ta FILA DERECHA Origen Abajo
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(3.5f, 0.25f, -1.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //4ta Fila Derecha Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(3.5f, 0.25f, 3.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.6f, 0.0f, 0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //4ta Fila Derecha  Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(3.5f, 0.25f, -3.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-0.6f, 0.0f, 0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //5ta FILA DERECHA Origen
              //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(4.5f, 0.25f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();




        //5ta FILA DERECHA Arriba
        //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(4.5f, 0.25f, 2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo 3
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 3
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2
        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();
     
        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

       /* model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();*/


        //5ta Fila Derecha  Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(4.5f, 0.25f, -2.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Castillo 4
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.25f, 0.20f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
        model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 3

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 4

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 5

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 6

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 7

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 8

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 9

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        //capa 10

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 11

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();


        //capa 12

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.4f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[12]->RenderMesh();

        // Techo 4
        // Capa 1
        model = glm::mat4(1.0);
        model = model_aux2;
        model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(-0.5f, 0.4f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.5f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        model = model_aux2;
        model = glm::translate(model, glm::vec3(1.0f, 0.0f,  0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Capa 2


        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();


        model = model_aux2;
        model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[14]->RenderMesh();


        //1er Rengln Derecho  Origen Bajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -0.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

//Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();


        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
         //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //2do Rengln Derecho  Origen Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 0.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

       //BARANDAL
        //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();
        

        //3er Rengln Derecho Abajo
      //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -1.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.6f, 0.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //5to Escaln Derecho Abajo
      //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -2.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //4to Escaln Derecho Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -3.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3er Escaln Derecho Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -4.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2do Escaln Derecho Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -5.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1er Escaln Derecho Abajo
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, -6.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();



        //4to Rengln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 1.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 6
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //Castillo Cuadrado
        //Capa 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.75f, -0.79f, 0.0f));
        model_aux = model;
        model = glm::scale(model, glm::vec3(0.75f, 0.665385f, 0.75f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //Capa 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux = glm::translate(model_aux, glm::vec3(0.0f, 0.9f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick223Texture.UseTexture();
        meshList[13]->RenderMesh();

        //BARANDAL
         //Blastrado 1
        model = glm::mat4(1.0);
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.1f, -0.05f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.7f, 0.415f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 3

        model = model_aux;
        model = glm::translate(model, glm::vec3(-0.6f, 0.0f, 0.6f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //5to Escaln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 2.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 5
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();
 
        //4to Escaln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 3.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 4
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();


        //3er Escaln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 4.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 3
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //2do Escaln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 5.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();
        //Capa 2
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.0f, 0.2f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

        model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //1er Escaln Derecho Arriba
       //Capa 1
        model = glm::mat4(1.0);
        model = glm::translate(model, glm::vec3(6.0f, 0.25f, 6.5f));
        model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
        //model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        brick241Texture.UseTexture();
        meshList[14]->RenderMesh();

        //BARANDAL
        //Blastrado 1
        model = model_aux;
        model = glm::translate(model, glm::vec3(0.35f, 0.075f, 0.125f));
        model = glm::scale(model, glm::vec3(0.4f, 0.6f, 0.4f));
        //model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();

        //Blastrado 2

       model = model_aux2;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -0.6f));
        model_aux2 = model;
        //model = glm::scale(model, glm::vec3(0.59f, 0.309f, 0.2f));
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
        roundBrickTexture.UseTexture();
        meshList[11]->RenderMesh();
}

void RenderBlackhawk()
{
    glm::mat4 model(1.0);
    glm::vec3 posblackhawk = glm::vec3(2.0f, 0.0f, 0.0f);

    model = glm::mat4(1.0);
    posblackhawk = glm::vec3(posXavion+ movAvion_x,posYavion+ movAvion_y, posZavion + movAvion_z);
    model = glm::translate(model, posblackhawk);
    model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
    model = glm::rotate(model, giroAvion* toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, -90* toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
    glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
    //Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
    Blackhawk_M.RenderModel();
}

