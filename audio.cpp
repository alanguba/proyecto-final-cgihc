//
//  audio.cpp
//  GLFW OpenGL
//
//  Created by Alan Gutiérrez on 02/05/20.
//  Copyright © 2020 Alan Gutiérrez. All rights reserved.
//

#include "audio.h"

Audio::~Audio() {
    SDL_CloseAudioDevice(deviceId);
    SDL_FreeWAV(wavBuffer);
}

void Audio::load(const char* filename) {
    SDL_LoadWAV(filename, &wavSpec, &wavBuffer, &wavLength);
    deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);
}

void Audio::play() {
    SDL_QueueAudio(deviceId, wavBuffer, wavLength);
    SDL_PauseAudioDevice(deviceId, 0);
}

Uint32 Audio::getBitsRemaining()
{
    return SDL_GetQueuedAudioSize(deviceId);
}
