#pragma once
#include<stdio.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>

class Window
{
public:
    Window();
    Window(GLint windowWidth, GLint windowHeight);
    int Initialise();
    GLfloat getBufferWidth() { return bufferWidth; }
    GLfloat getBufferHeight() { return bufferHeight; }
    GLfloat getXChange();
    GLfloat getYChange();
    GLfloat getmuevex() { return muevex; }
    GLfloat getmuevez() { return muevez; }
    GLfloat getmueveluz1x() { return mueveluz1x; }
    GLfloat getmueveluz1y() { return mueveluz1y; }
    GLfloat getmueveluz1z() { return mueveluz1z; }
    GLfloat getmueveluz2x() { return mueveluz2x; }
    GLfloat getmueveluz2y() { return mueveluz2y; }
    GLfloat getmueveluz2z() { return mueveluz2z; }
    bool getOn() { return on_off; }
    bool getShouldClose() {
        return  glfwWindowShouldClose(mainWindow);}
    bool* getsKeys() { return keys; }
    void swapBuffers() { return glfwSwapBuffers(mainWindow); }
    
    ~Window();
private:
    GLFWwindow *mainWindow;
    GLint width, height;
    bool keys[1024];
    GLint bufferWidth, bufferHeight;
    void createCallbacks();
    GLfloat lastX;
    GLfloat lastY;
    GLfloat xChange;
    GLfloat yChange;
    GLfloat muevex;
    GLfloat muevez;
    bool on_off;
    GLfloat mueveluz1x;
    GLfloat mueveluz1y;
    GLfloat mueveluz1z;
    GLfloat mueveluz2x;
    GLfloat mueveluz2y;
    GLfloat mueveluz2z;
    bool mouseFirstMoved;
    static void ManejaTeclado(GLFWwindow* window, int key, int code, int action, int mode);
    static void ManejaMouse(GLFWwindow* window, double xPos, double yPos);

};

