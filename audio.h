//
//  audio.h
//  GLFW OpenGL
//
//  Created by Alan Gutiérrez on 02/05/20.
//  Copyright © 2020 Alan Gutiérrez. All rights reserved.
//

#ifndef audio_h
#define audio_h

#include <SDL2/SDL.h>

class Audio {
public:
    ~Audio();
    void load(const char* filename);
    void play();
    Uint32 getBitsRemaining();
protected:
    SDL_AudioSpec wavSpec;
    Uint32 wavLength;
    Uint8 *wavBuffer;
    SDL_AudioDeviceID deviceId;
};

#endif /* audio_h */
